import React from 'react';
import {ImageBackground, ScrollView,SafeAreaView,Text,Image, View,TouchableOpacity} from 'react-native';
import {windowWidth} from '../../Util/Dimensions';
import { Icon } from 'react-native-elements'
// import { Dimensions } from 'react-native';
export default function About({ navigation}) {
//     const windowWidth = Dimensions.get('window').width;
// const windowHeight = Dimensions.get('window').height;
  return (
    <SafeAreaView>
    <ScrollView>
    <View style={{padding:20,flexDirection: 'column',justifyContent: 'space-around',marginBottom:20}}>
    {/* <View style={{justifyContent: 'flex-start',alignItems: 'flex-start'}}>
    <TouchableOpacity onPress={() => navigation.navigate('InfoStack')}><Icon
            name='arrow-back' 
              size={40}
            /></TouchableOpacity>
    </View> */}
        <View style={{justifyContent: 'center',alignItems: 'center'}}>
            <Image 
                style={{width:180, height: 180}}
                source={require('../../../assets/circleBJYM.png')}
            />
            <Text style={{textAlign: 'center',fontSize: 30,fontWeight: 'bold',marginTop:36}}>Bharatiya Janta Yuva Morcha</Text>
            <Text style={{textAlign: 'center',
                           fontSize:20,color:'grey',
                           fontWeight: 'bold',
                           marginTop:10}}>Nagpur</Text>
        </View>
        <View style={{justifyContent: 'center', alignItems: 'center',marginTop:20}}>
            <Text style={{fontWeight: 'bold',fontSize: 26}}>About Us</Text>
        </View>
        <View style={{marginTop:20}}>
        <Text style={{fontSize:18,textAlign: 'center',lineHeight:25}}>The concept of Integral Humanism has a special place in its ideology, 
        with the party aiming to transform India into a modern, 
        progressive and enlightened nation which draws inspiration from India’s ancient Indian culture and values. 
        Being the youth wing of the BJP, BJYM mainly focuses on the issues related to country’s youth. 
        BJYM believes in empowering the youth and giving them a platform where they are engaged and their 
        aspirations and sentiments are reflected in the policies of the BJP.</Text>
        </View>
        <View style={{flexDirection: 'row',justifyContent: 'space-between',marginTop:25}}>
        <View style={{flexDirection: 'column',justifyContent: 'center',alignItems: 'center'}}>
        <Image
                source={require('../../../assets/mohan.jpg')}
                style={{width: 80, height: 80, borderRadius: 50}}
              />
              <Text style={{marginTop:10}}>Shri Mohan Ji </Text>
              <Text style={{alignItems: 'center'}}>Mate</Text>
        </View>   
        <View style={{justifyContent: 'center',alignItems: 'center'}}>
                <Image
                source={require('../../../assets/deve.jpg')}
                style={{width: 80, height: 80, borderRadius: 50}}
              />
              <Text style={{marginTop:10}}>Shri Devendra Ji </Text>
              <Text style={{alignContent: 'center'}}>Fadnavis</Text>
        </View>
        <View style={{justifyContent: 'center',alignItems: 'center'}}>
                <Image
                source={require('../../../assets/ParendraPatle.jpeg')}
                style={{width: 80, height: 80, borderRadius: 50}}
              /> 
              <Text style={{marginTop:10}}>Shri Parendra Ji </Text>
              <Text style={{alignItems: 'center'}}>Patle</Text>
        </View>
        </View>
        
    </View>
    </ScrollView>
    </SafeAreaView>
  );
}