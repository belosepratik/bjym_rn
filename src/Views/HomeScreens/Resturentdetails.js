import React from 'react'
import { Text, FlatList, View, TouchableOpacity, Image } from 'react-native'
import Wrapped from '../../Components/Global/Wrapped'

import Font from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import CommonHeader from '../../Components/Global/CommonHeader';
export default function Resturentdetails({ route, navigation }) {

    const HandleFood = (name, img, address) => {
        navigation.navigate('FoodSectionScreenwtab', { "name": name, "img": img, "address": address })
    }
    return (
        <Wrapped>
            <CommonHeader back={true} name="Restaurants" navigation={navigation} />
            <FlatList
                data={route.params.data}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={{ marginTop: 20 }}
                ItemSeparatorComponent={(props) => {
                    // console.log('props', props); // here you can access the trailingItem with props.trailingItem
                    return (<View style={{ marginTop: 10, }} />);
                }}
                renderItem={({ item }) => (
                    <>
                        <TouchableOpacity onPress={() => HandleFood(item.name, item.img, item.address)}
                            style={{ marginLeft: 10, flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                style={{
                                    width: 80,
                                    height: 80,
                                    borderRadius: 10,
                                    marginRight: 10
                                }}
                                source={{
                                    uri: item.img,
                                }}
                            >
                            </Image>
                            <View>
                                <Text style={{ margin: 10, fontSize: 18, fontWeight: 'bold' }}>{item.name}</Text>
                                <View style={{ flexDirection: 'row', }}>
                                    <Font style={{ marginLeft: 10, }} name="star" size={15} color="#ffe234" />
                                    <Font style={{ marginLeft: 10, }} name="star" size={15} color="#ffe234" />
                                    <Font style={{ marginLeft: 10, }} name="star" size={15} color="#ffe234" />
                                    <Font style={{ marginLeft: 10, }} name="star" size={15} color="#ffe234" />
                                    <Font style={{ marginLeft: 10, }} name="star" size={15} color="#ffe234" />
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                                    <MaterialCommunityIcons name="clock-time-five-outline" size={15} color="grey" />
                                    <Text style={{ color: 'grey', marginLeft: 5 }}>{item.time}</Text>
                                </View>

                            </View>

                        </TouchableOpacity>
                        <View style={{ borderWidth: 0.2, marginTop: 5 }} />
                    </>
                )}

            />
        </Wrapped>
    )
}
