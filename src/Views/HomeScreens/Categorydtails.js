
import React from 'react'
import { FlatList, Image, Text, TouchableOpacity, View } from 'react-native'
import { useSelector } from 'react-redux'
import CommonHeader from '../../Components/Global/CommonHeader'
import Wrapped from '../../Components/Global/Wrapped'

export default function Categorydtails({ route, navigation }) {
    // console.log(route)
    const home = useSelector(state => state.home)
    const {categories,restaurants}=home
    const HandleCategory = (id, name) => {
        console.log(id, name)
        navigation.navigate('Category', { "id": id, "name": name, "cat": categories, "restro": restaurants })
    }
    return (
        <Wrapped>
            <CommonHeader back={true} name="Category" navigation={navigation} />
            <FlatList
                data={route.params.data}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={{ marginTop: 15, }}
                ItemSeparatorComponent={(props) => {
                    // console.log('props', props); // here you can access the trailingItem with props.trailingItem
                    return (<View style={{ marginTop: 10, }} />);
                }}
                renderItem={({ item }) => (
                    <>
                        <TouchableOpacity style={{ marginLeft: 10, marginBottom: 20, marginRight: 10, flexDirection: 'row', alignItems: 'center' }}
                            onPress={() =>HandleCategory(item.id, item.name)} >
                            <Image
                                source={{ uri: item.icon }}
                                style={{ width: 50, height: 50, borderRadius: 10 }}
                            />

                            <View style={{ marginLeft: 10 }}>
                                <Text style={{ marginTop: 5, textAlign: "center", fontSize: 16, }}>{item.name}</Text>
                                <Text style={{ marginTop: 5, textAlign: "center" }}>{item.veg}</Text>
                            </View>

                        </TouchableOpacity>
                        <View style={{ borderWidth: 0.2, marginTop: 0 }} />
                    </>

                )}
            />
        </Wrapped>
    )
}
