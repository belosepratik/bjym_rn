import React, { useState } from 'react'
import { ScrollView, Text, StatusBar } from 'react-native'
import { View, Image } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import ImageBackground from 'react-native/Libraries/Image/ImageBackground'
import RestroCategoryComp from '../../../Components/AppComponents/CategoryComponents/RestroCategoryComp'
import Wrapped from '../../../Components/Global/Wrapped'

export default function Category({ route, navigation }) {
    const { name, restro, cat } = route.params
    const HandleRestro = (item) => {
        navigation.navigate('Restaurents', { "data": item })
    }
    // console.log("restro", restro)
    const [loading, setloading] = useState(false)

    const [restro1, setrestro1] = useState([])
    let arr = []
    React.useEffect(() => {
        try {
            setloading(true)
            // console.log(restaurants)
            restro.map((d, i) => {
                arr.push({
                    "name": d.name,
                    "id": d._id,
                    "add": false,
                    "left": true,
                    "dev": "50% OFF upto ₹1000 ",
                    "price": '₹18,500',
                    "discount": "50% OFF",
                    "button": false,
                    "top": true,
                    "rating": 4.0,
                    "health": "Healthy Anterantives available",
                    "address": `${d.addr1} ${d.addr2} ${d.addr3} ${d.addr4}`,
                    "city": d.city,
                    "distance": "35 kms"
                })
            })
            setrestro1(arr)
            console.log(arr)
            setloading(false)
        } catch (e) {
            console.log(e)
            setloading(false)
        }
    }, [])
    return (
        <Wrapped >
            <ScrollView >
                <ImageBackground source={require('../../../../assets/FoodCover.jpg')} resizeMode='cover'
                    style={{ width: '100%', height: 150, }} />
                <View style={{ marginLeft: 20, marginRight: 20 }}>
                    <View style={{ marginTop: 20 }}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold' }}>{name}</Text>
                    </View>
                    <View style={{ marginTop: 20, borderWidth: 0.3 }} />
                    {
                        loading ?
                            <Skeleton />
                            :
                            <RestroCategoryComp HandleRestro={HandleRestro} restro={restro1} addbutton={false} navigation={navigation}/>
                    }
                </View>
            </ScrollView>


        </Wrapped >
    )
}
