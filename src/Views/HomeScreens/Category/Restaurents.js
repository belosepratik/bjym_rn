import React from 'react'
import { Text, View, StatusBar } from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign';
import { ScrollView } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-native-safe-area-context'
import Ent from 'react-native-vector-icons/Entypo'
import RatingsComponent from '../../../Components/AppComponents/RestroComponents/RatingsComponent'
import { windowHeight } from '../../../Util/Dimensions'
import { List } from 'react-native-paper'
import FoodAccordian from '../../../Components/AppComponents/RestroComponents/FoodAccordian'
import CommonHeader from '../../../Components/Global/CommonHeader';
import Wrapped from '../../../Components/Global/Wrapped';
import BottomCartButton from '../../../Components/Global/BottomCartButton';
import { useSelector } from 'react-redux';

const DataObject = [
    {
        "name": "Recommented",
        "id": 1,
        "food": [
            {
                "seller": 'Bestseller',
                "item": "Chicken Salad",
                "price": 1267,
                "id": 101
            },
            {
                "seller": 'Bestseller',
                "item": "Chicken Crispy",
                "id": 102,
                "price": 267,
            },
        ]
    },
    {
        "name": "Sandwiches",
        "id": 2,
        "food": [
            {
                "seller": 'Bestseller',
                "item": "Sandwich Salad",
                "id": 103,
                "price": 1867,
            },
            {
                "seller": 'Bestseller',
                "item": "Sandwich Crispy",
                "id": 104,
                "price": 1277,
            },
            {
                "seller": 'Bestseller',
                "item": "Sandwich",
                "id": 105,
                "price": 157,
            },
        ]
    },
    {
        "name": "Burger",
        "id": 3,
        "food": [
            {
                "seller": 'Bestseller',
                "item": "Burger Salad",
                "id": 106,
                "price": 128,
            },
            {
                "seller": 'Bestseller',
                "item": "Burger Crispy",
                "id": 107,
                "price": 1467,
            },
        ]
    },
    {
        "name": "Pizza",
        "id": 4,
        "food": [
            {
                "seller": 'Bestseller',
                "item": "Pizza Salad",
                "id": 108,
                "price": 1867,
            },
            {
                "seller": 'Bestseller',
                "item": "Pizza Crispy",
                "id": 109,
                "price": 1777,
            },
        ]
    },


]


export default function Restaurents({ route, navigation }) {
    console.log(route)
    const cart = useSelector(state => state.cart)
    const { items } = cart
    const HandleAdd = () => {
        navigation.navigate('Cart',{"name":name,"address":address})
    }

    const { name, price, discount, rating, health, address, city, distance } = route.params.data

    return (
        <Wrapped >
            <ScrollView>
                <CommonHeader navigation={navigation} name={name} back={true} />
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 5 }}>
                    <View>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', marginTop: 10 }}>{name}</Text>
                        <Text style={{ color: 'grey', fontSize: 16, marginTop: 5 }}>{health}</Text>
                        <Text style={{ color: 'grey', fontSize: 16, marginTop: 5 }}>{city} | {distance}</Text>
                    </View>
                    <View style={{ marginTop: 20, borderWidth: 0.3 }} />
                    <RatingsComponent data={route.params.data} />
                    <View style={{ marginTop: 20, borderWidth: 0.3 }} />
                    <FoodAccordian data={DataObject}/>
                </View>
            </ScrollView>
            {
                items.length !=0 &&<BottomCartButton noitem={items.length} Messgae="View Cart" page="hometocart"  HandleAdd={HandleAdd} tab={false}/>
            }
            
        </Wrapped>
    )
}
