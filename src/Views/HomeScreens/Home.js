import React, {useState} from 'react';
import Header from '../../Components/AppComponents/HomeComponents/Header';
import {
  View,
  Image,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  True,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  // ActivityIndicator,
} from 'react-native';

import Location from '../../Components/AppComponents/HomeComponents/Location';
import Category from '../../Components/AppComponents/HomeComponents/Category';
import Food from '../../Components/AppComponents/HomeComponents/Food';
import FoodOffers from '../../Components/AppComponents/HomeComponents/FoodOffers';
import SearchBar from '../../Components/Global/SearchBar';
import {useDispatch, useSelector} from 'react-redux';
import Geocoder from 'react-native-geocoding';
import {set} from 'react-native-reanimated';
import {GeoDecoding} from '../../functions/GeoDecording';
import ApiCalls from '../../Api/Api';
import {useIsFocused} from '@react-navigation/native';
import Skeleton from '../../Components/Skeleton/Skeleton';
import Wrapped from '../../Components/Global/Wrapped';
import {
  setcategories,
  setfoods,
  setrestaurants,
} from '../../Redux/Slice/HomeSlice';
import {setitems} from '../../Redux/Slice/CartSlice';
import CommonHeader from '../../Components/Global/CommonHeader';
import HomeNewsComp from '../../Components/BJYM/HomeNewsComp';
import Banner from '../../Components/BJYM/Banner';

import Input from '../../Components/Basic/Input';
import Fontisto from 'react-native-vector-icons/Fontisto';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Config} from '../../../Config';
import {ActivityIndicator} from 'react-native-paper';
import {setIsLogged, setUser} from '../../Redux/Slice/LoginSlice';
const call = new ApiCalls();
function Home({navigation, route}) {
  const searchData = (txt) => {
    console.log(txt);
  };

  const [loading, setloading] = useState(false);
  const [msg, setmsg] = useState('');

  const [name, setname] = useState('Name is required');
  const [emailerr, setemailerr] = useState('Email is required');
  const [bloodgrperr, setbloodgrperr] = useState('Blood is required');
  const [passerr, setpasserr] = useState('Password is required');
  const [phoneerr, setphoneerr] = useState('Mobile Number is required');
  const [mobileerr, setmobileerr] = useState('Alt Mobile Number is required');
  const [usererr, setusererr] = useState('Username is required');
  const [constituencyerr, setconstituencyerr] = useState(
    'Constituency is required',
  );
  const [addresserr, setaddresserr] = useState('Address is required');
  const [valid, setvalid] = useState(false);
  const [state, setstate] = useState({
    name: '',
    mobile: '',
    mobileAlt: '',
    bloodgrp: '',
    address: '',
    constituency: '',
  });
 
  const dispatch = useDispatch();

  let res = [];
  const HandleRegister = async () => {
    setvalid(true);
    try {
      setloading(true);

      console.log(
        'HandleRegister',
        name == null,
        phoneerr == null,
        constituencyerr == null,
        addresserr == null,
        mobileerr == null,
      );
      if (
        name == null &&
        // bloodgrperr == null &&
        phoneerr == null &&
        constituencyerr == null &&
        addresserr == null &&
        mobileerr == null
      ) {
        const data = {
          username: state.mobile,
          password: `${state.mobile}`,
          name: state.name,
          mobile: state.mobile,
          mobileAlt: state.mobileAlt,
          bloodgrp: state.bloodgrp,
          address: state.address,
          constituency: state.constituency,
          roles: 'user',
        };

        res = await call.Calls('bjp', 'POST', data, Config);
        if (res.status == '200') {
          // console.log(res)
          dispatch(setUser(res.data));
          dispatch(setIsLogged(true));
          navigation.replace('Home');
          // navigation.replace(route.params.screen)
        } else {
          // console.log(res)
          console.log(res.msg.response.data.message);
          alert(res.msg.response.data.message);
        }
      }
      console.log(state);

      console.log('res', res);
      setloading(false);
    } catch (e) {
      console.log(e);
      setloading(false);
    }
  };

  const Validate = (e, msg) => {
    // console.log(e, msg)
    const regexname = new RegExp(/^[a-zA-Z ]+$/);
    const regexaddress = new RegExp(/^[a-zA-Z0-9\s,'-]*$/);
    const regexEmail = new RegExp(
      /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/,
    );
    const regexPhone = new RegExp(
      /^[+]?(\d{1,2})?[\s.-]?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
    );
    switch (msg) {
      case 'name':
        if (e.length === 0) {
          setname('Name is required');
        } else if (!regexname.test(e)) {
          setname('Invalid first name');
        } else {
          setname();
        }
        break;
      case 'username':
        if (e.length === 0) {
          setusererr('username is required ');
        } else {
          setusererr();
        }
        break;
      case 'email':
        if (e.length === 0) {
          setemailerr('Email is required');
        } else if (!regexEmail.test(e)) {
          setemailerr('Invalid email');
        } else {
          setemailerr();
        }
        break;
      case 'pass':
        if (e.length === 0) {
          setpasserr('Password is required ');
        } else if (e.length <= 5) {
          setpasserr("'Password is too small");
        } else {
          setpasserr();
        }
        break;
      case 'phone':
        if (e.length === 0) {
          setphoneerr('Mobile Number is required ');
        } else if (!regexPhone.test(e)) {
          setphoneerr('Invalid Mobile number');
        } else {
          setphoneerr();
        }
        break;
      case 'mobile':
        if (e.length === 0) {
          setmobileerr('Alt Mobile Number is required ');
        } else if (!regexPhone.test(e)) {
          setmobileerr('Invalid mobile number');
        } else {
          setmobileerr();
        }
        break;
      case 'bloodgrp':
        if (e.length === 0) {
          setbloodgrperr('Blood Group is required ');
        } else if (!regexname.test(e)) {
          setbloodgrperr('Invalid Blood Group');
        } else {
          setbloodgrperr();
        }
        break;
      case 'address':
        if (e.length === 0) {
          setaddresserr('Address is required ');
        } else if (!regexaddress.test(e)) {
          setaddresserr('Invalid Address');
        } else {
          setaddresserr();
        }
        break;
      case 'constituency':
        if (e.length === 0) {
          setconstituencyerr('Constituency is required ');
        } else if (!regexaddress.test(e)) {
          setconstituencyerr('Invalid Constituency');
        } else {
          setconstituencyerr();
        }
        break;
    }
  };
  return (
    <Wrapped>
      <CommonHeader name="Welcome To BJYM South Nagpur" back={false} />

      <ScrollView keyboardShouldPersistTaps="always">
      <Image 
        source={require('../../../assets/tiranga.png')}
        position="absolute"
        style={{height:'100%', width:'100%'}}
      />
               <Banner />
        <View style={{backgroundColor: '#F6F0F0 ', margin: 20, marginTop: 10}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              // marginTop: 20,
            }}>
            <AntDesign
              name="user"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Name *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'name');
                setstate({...state, name: e});
              }}
            />
          </View>
          {valid && name && (
            <Text style={{marginLeft: 30, color: 'red'}}>{name}</Text>
          )}
          {/* <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20, marginTop: 20 }}>Login</Text> */}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <AntDesign
              name="phone"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Mobile No*"
              keyboardType="numeric"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'phone');
                setstate({...state, mobile: e});
              }}
            />
          </View>
          {valid && phoneerr && (
            <Text style={{marginLeft: 30, color: 'red'}}>{phoneerr}</Text>
          )}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <AntDesign
              name="phone"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Alt Mobile No*"
              keyboardType="numeric"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'mobile');
                setstate({...state, mobileAlt: e});
                
              }}
            />
          </View>
          {valid && mobileerr && (
            <Text style={{marginLeft: 30, color: 'red'}}>{mobileerr}</Text>
          )}

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <Fontisto
              name="blood-drop"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Blood Group *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                // Validate(e, 'bloodgrp');
                setstate({...state, bloodgrp: e});
              }}
            />
          </View>

          {/* {valid && bloodgrperr && (
            <Text style={{marginLeft: 30, color: 'red'}}>{bloodgrperr}</Text>
          )} */}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <Entypo
              name="address"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Address *"
              // secureTextEntry={true}
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'address');
                setstate({...state, address: e});
              }}
            />
          </View>
          {valid && addresserr && (
            <Text style={{marginLeft: 30, color: 'red'}}>{addresserr}</Text>
          )}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <MaterialCommunityIcons
              name="vote"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Your Constituency *"
              // maxLength={10}
              // keyboardType="numeric"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'constituency');
                setstate({...state, constituency: e});
              }}
            />
          </View>

          {valid && constituencyerr && (
            <Text style={{marginLeft: 30, color: 'red'}}>
              {constituencyerr}
            </Text>
          )}
          <TouchableOpacity
            onPress={() => HandleRegister()}
            style={{
              margin: 20,
              padding: 10,
              backgroundColor: '#f06b32',
              borderRadius: 10,
            }}>
            {loading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text style={{alignSelf: 'center', fontSize: 20,color:"#fff",fontWeight:'bold'}}>Join BJYM</Text>
            )}
          </TouchableOpacity>
        </View>

        {/* <SearchBar Handlesearch={searchData} /> */}
        {/* <HomeNewsComp /> */}
      </ScrollView>

      {/* </Header> */}
    </Wrapped>
  );
}
export default Home;
