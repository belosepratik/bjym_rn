import React, { useState } from 'react'
import { Text, ScrollView, TouchableOpacity, View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useDispatch, useSelector } from 'react-redux'
import ApiCalls from '../../Api/Api'
import FoodSectionHeader from '../../Components/AppComponents/HomeComponents/FoodSectionComponents/FoodSectionHeader'
import FoodTabSectionComponent from '../../Components/AppComponents/HomeComponents/FoodSectionComponents/FoodTabSectionComponent'
import FoodTabSectionHeader from '../../Components/AppComponents/HomeComponents/FoodSectionComponents/FoodTabSectionHeader'
import BottomCartButton from '../../Components/Global/BottomCartButton'
import Wrapped from '../../Components/Global/Wrapped'
import { setfoods } from '../../Redux/Slice/HomeSlice'


const FootTab = [
    {
        "name": "Fruit Mix",
        "id": 1,
        "add": false,
        "left": true,
        "dev": "delivery ",
        "price": '18,500',
        "button": true,


    },
    {
        "name": "Fruit Mix",
        "id": 2,
        "add": true,
        "left": false,
        "dev": "delivery discount up to 3x",
        "price": '22,500',
        "button": true,
    },
    {
        "name": "Fruit Mix",
        "id": 3,
        "add": true,
        "left": false,
        "dev": "delivery ",
        "price": '18,500',
        "button": true,
    },
    {
        "name": "Fruit Mix",
        "id": 4,
        "add": true,
        "left": false,
        "dev": "delivery discount up to 3x",
        "price": '22,500',
        "button": true,
    },
]

const call = new ApiCalls()
export default function FoodSectionScreen({ route, navigation }) {
    console.log(route)
    const cart = useSelector(state => state.cart)
    const { items } = cart
    const { img, name, address } = route.params
    const [food, setfood] = useState([])
    const [utab, setutab] = useState(0)
    const [loading, setloading] = useState(false)
    const dispatch = useDispatch()
    let arr = []

    const HandleTabs = (value) => {
        setutab(value)
    }

    const HandleAdd = () => {
        navigation.navigate('CartStacks', { "name": name, "address": address })

    }
    React.useEffect(() => {

        (async () => {
            try {
                setloading(true)
                let fooddata = await call.Calls('food', 'GET')
                dispatch(setfoods(fooddata.data))
                fooddata.data.results.map((d, i) => {
                    arr.push({
                        "name": d.name,
                        "id": d._id,
                        "add": true,
                        "left": true,
                        "dev": "delivery ",
                        "price": 250,
                        "item": d.name,
                        "button": true,

                    })
                })
                setfood(arr)

                setloading(false)
            } catch (e) {
                console.log(e)
            }

        })()
    }, [])
    return (
        <Wrapped>
            <ScrollView>
                <FoodSectionHeader img={img} name={name} address={address} navigation={navigation} />
                <FoodTabSectionComponent header={true} FootTab={food} addbutton={true} name={name} address={address} HandleTabs={HandleTabs} tabs={utab} />
            </ScrollView>
            {
                utab ==0 &&
                items.length != 0 && <BottomCartButton noitem={items.length} Messgae="View Cart" page="hometocart" HandleAdd={HandleAdd} tab={route.name == 'FoodSectionScreenwtab' ?false :true} />
            }

        </Wrapped>
    )
}
