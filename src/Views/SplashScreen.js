import {useField} from 'formik';
import React, {useEffect} from 'react';
import {
  Image,
  View,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  Text,
  StyleSheet,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {setIsLogged, setUser} from '../Redux/Slice/LoginSlice';

import Wrapped from '../Components/Global/Wrapped';
import {windowHeight, windowWidth} from '../Util/Dimensions';

function SplashScreen({navigation}) {
  const login = useSelector((state) => state.login);
  const {isLogged} = login;
  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      dispatch(setIsLogged(true));
      navigation.replace('Home');
      // isLogged ? navigation.replace('Home') : navigation.replace('Login');
    }, 3000);
  }, []);

  return (
    <Wrapped>
    <View style={{flex:1,flexDirection: 'column',justifyContent: 'space-evenly',alignItems: 'center'}}>
        <View>
        <Text style={{
            textAlign: 'center',
            fontSize: 25,
            color: '#F97D09',
            fontWeight: 'bold'}}>Bharatiya Janta Yuva Morcha</Text>
        </View>
        <View>
        <Image
            source={require('../../assets/circleBJYM.png')}
            style={{width: 240, height: 240}}
          />
        </View>
        <View>
        <Text
          style={{
            fontSize: 30,
            textAlign: 'center',
          }}>
          Initiative By
          </Text>
        </View>

      <View>
          <View style={{ flexDirection: 'row',}}>
            <View style={{
                backgroundColor: 'white',
                height: 100,
                width: 100,
                borderRadius: 60,
                alignItems: 'center',
                justifyContent: 'center',elevation: 5,}}>
                 <Image
                source={require('../../assets/nitingadkari.png')}
                style={{width: 80, height: 80, borderRadius: 50,}}
              />
            </View>
            <View style={{
                backgroundColor: 'white',
                height: 100,
                width: 100,
                marginLeft:'5%',
                borderRadius: 60,
                alignItems: 'center',
                justifyContent: 'center',elevation: 5,}}>
             <Image
                source={require('../../assets/deve.jpg')}
                style={{width: 80, height: 80, borderRadius: 50}}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row',justifyContent:'space-around'}}>
             <View>
                <Text>Shri Nitin Ji</Text>
              <Text style={{textAlign: 'center',}}>Gadkari</Text>
                </View>
              <View>
                <Text>Shri Devendra Ji</Text>
              <Text style={{textAlign: 'center',}}>Fadnavis</Text>
              </View>
              </View>
      </View>

      <View>
          <View style={{ flexDirection: 'row',}}>
            <View style={{
                backgroundColor: 'white',
                height: 100,
                width: 100,
                borderRadius: 60,
                alignItems: 'center',
                justifyContent: 'center',elevation: 5,}}>
                 <Image
               source={require('../../assets/mohan.jpg')}
                style={{width: 80, height: 80, borderRadius: 50,}}
              />
            </View>
            <View style={{
                backgroundColor: 'white',
                height: 100,
                width: 100,
                marginLeft:'5%',
                borderRadius: 60,
                alignItems: 'center',
                justifyContent: 'center',elevation: 5,}}>
             <Image
                source={require('../../assets/ParendraPatle.jpeg')}
                style={{width: 80, height: 80, borderRadius: 50}}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row',justifyContent:'space-around'}}>
             <View>
                <Text>Shri Mohan Ji</Text>
              <Text style={{textAlign: 'center',}}>Mate</Text>
                </View>
              <View>
                <Text>Shri Parendra Ji</Text>
              <Text style={{textAlign: 'center',}}>Patle</Text>
              </View>
              </View>
      </View>
      
      <View style={{flexDirection:'column',alignItems: 'center'}}>
          <Text style={{textAlign:'center',}}>Developed by Anto & Team ECS [www.exits.in]</Text>
      </View>
    </View>    
    </Wrapped>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 25,
    padding: 15,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  contentCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    color: 'white',
    padding: 10,
  },
});
export default SplashScreen;