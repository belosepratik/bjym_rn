import React, { useState } from 'react'
import { ScrollView, Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useDispatch, useSelector } from 'react-redux'
import Category from '../../Components/AppComponents/HomeComponents/Category'
import Food from '../../Components/AppComponents/HomeComponents/Food'
import FoodOffers from '../../Components/AppComponents/HomeComponents/FoodOffers'
import FoodTabSectionComponent from '../../Components/AppComponents/HomeComponents/FoodSectionComponents/FoodTabSectionComponent'
import FoodListSearchComponents from '../../Components/AppComponents/SearchComponents/FoodListSearchComponents'
import MenuBoxButtonSearch from '../../Components/AppComponents/SearchComponents/MenuBoxButtonSearch'
import BottomCartButton from '../../Components/Global/BottomCartButton'
import SearchBar from '../../Components/Global/SearchBar'
import Wrapped from '../../Components/Global/Wrapped'
import { setitems } from '../../Redux/Slice/CartSlice'
import { setfood } from '../../Redux/Slice/HomeSlice'
const data = [
  {
    "name": "Pure Veg",
    "id": 1
  },
  {
    "name": "Rate 4+",
    "id": 2
  },
  {
    "name": "Less Than 45mins",
    "id": 3
  },
  {
    "name": "Rs 300-Rs 600",
    "id": 4
  },
  {
    "name": "Pure Veg",
    "id": 5
  },
  {
    "name": "Pure Veg",
    "id": 6
  },
  {
    "name": "Pure Veg",
    "id": 7
  },
]




const FootTab = [
  {
    "name": "Fruit Mix",
    "id": 1,
    "add": false,
    "left": true,
    "dev": "delivery ",
    "price": '18,500'

  },
  {
    "name": "Fruit Mix",
    "id": 2,
    "add": true,
    "left": true,
    "dev": "delivery discount up to 3x",
    "price": '22,500'
  },
  {
    "name": "Fruit Mix",
    "id": 3,
    "add": true,
    "left": true,
    "dev": "delivery ",
    "price": '18,500',
    "top": true,
  },
  {
    "name": "Fruit Mix",
    "id": 4,
    "add": true,
    "left": true,
    "dev": "delivery discount up to 3x",
    "price": '22,500',
    "top": true,
  },
  {
    "name": "Fruit Mix",
    "id": 5,
    "add": true,
    "left": true,
    "dev": "delivery discount up to 3x",
    "price": '22,500',
    "top": true,
  },
  {
    "name": "Fruit Mix",
    "id": 6,
    "add": true,
    "left": true,
    "dev": "delivery discount up to 3x",
    "price": '22,500',
    "top": true,
  },
  {
    "name": "Fruit Mix",
    "id": 7,
    "add": true,
    "left": true,
    "dev": "delivery discount up to 3x",
    "price": '22,500',
    "top": true,
  },
]


const CRT = [
  {
    "id": 1,
    "name": "Restaurants",
  },
  {
    "id": 2,
    "name": "Dishes"
  },
]
export default function Search({ navigation,route }) {

  const cart = useSelector(state => state.cart)
  const { items } = cart

  const home = useSelector(state => state.home)
  const { restaurants, foods } = home

  const dispatch = useDispatch()

  const [restro, setrestro] = useState([])
  const [select, setselect] = useState(1)
  const [cat, setcat] = useState(1)
  const [food, setfood] = useState([])
  const [search, setsearch] = useState()
  const [fsearch, setfsearch] = useState()
  let arr = []
  let arr1 = []

  React.useEffect(() => {
    dispatch(setitems([]))
  }, [select])

  React.useEffect(() => {
    restaurants.results.map((d, i) => {
      arr.push({
        "name": d.name,
        "id": d._id,
        "add": false,
        "left": true,
        "dev": "50% OFF upto ₹1000 ",
        "price": '₹18,500',
        "discount": "50% OFF",
        "button": false,
        "top": true,
        "rating": 4.0,
        "health": "Healthy Anterantives available",
        "address": `${d.addr1} ${d.addr2} ${d.addr3} ${d.addr4}`,
        "city": d.city,
        "distance": "35 kms"
      })
    })
    setrestro(arr)
    setsearch(arr)
  }, [])


  React.useEffect(() => {
    try {
      foods.results.map((d, i) => {
        arr1.push({
          "name": d.name,
          "id": d._id,
          "add": true,
          "left": true,
          "dev": "delivery ",
          "price": 250,
          "item": d.name,
          "button": true,

        })
      })
      setfood(arr1)
      setfsearch(arr1)
      // console.log(arr)

    } catch (e) {
      console.log(e)
    }

  }, [])

  const HandleSelect = (value) => {
    setselect(value)
  }

  const HandleCat = (value) => {
    setcat(value)
  }

  // const Handlesearch = (value) => {
  //   setsearch(value)
  //   console.log("value", value)

  // }
  const searchData = (text) => {
    const newData = (select == 1 ? search : fsearch).filter((item) => {
      const itemData = item.name.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    select == 1 ? setrestro(newData) : setfood(newData);

  };
  

  const HandleAdd = () => {
    navigation.navigate('CartStacks', { "name": "test", "address": "test test test" })
  }
  const HandleRestro = (item) => {
    navigation.navigate('Restaurents', { "data": item })
  }
  return (
    <Wrapped>
      <ScrollView >
        <SearchBar place="Search Restaurants or Food" SearchTab={true} Handlesearch={searchData} />
        <MenuBoxButtonSearch data={data} CRT={CRT} HandleSelect={HandleSelect} select={select} HandleCat={HandleCat} cat={cat} />
        {
          select == 1 ? <FoodListSearchComponents HandleRestro={HandleRestro} FootTab={restro} select={select} />
            : <FoodTabSectionComponent header={false} FootTab={food} addbutton={true} tabs={0} />
        }

      </ScrollView>
      {select == 2 &&
        <>
          {

            items.length != 0 && <BottomCartButton noitem={items.length} Messgae="View Cart" page="hometocart" HandleAdd={HandleAdd} tab={true} />

          }
        </>}
    </Wrapped>
  )
}
