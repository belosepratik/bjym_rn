import React from 'react';
import {ImageBackground, Text, View, TouchableOpacity} from 'react-native';
import {windowWidth} from '../../Util/Dimensions';
import Info from 'react-native-vector-icons/Entypo';
import Telescope from 'react-native-vector-icons/Octicons';
import Target from 'react-native-vector-icons/SimpleLineIcons';
import Peoples from 'react-native-vector-icons/Ionicons';
import EventNote from 'react-native-vector-icons/MaterialIcons';


export default function InfoScreen({navigation}) {
  return (
    <>
      <View
        style={{
          padding: 15,
          flexDirection: 'row',
        }}>
        <View
          style={{
            flex: 0.5,
            flexDirection: 'column',
            backgroundColor: '#FFF',
            borderRadius: 10,
            margin: 3,
            padding: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              
            }}>
            <TouchableOpacity onPress={() =>navigation.navigate('About')}>
            <Info
              name="info"
              size={25}
              color="#1e9143"
              style={{marginBottom: 5,alignSelf: 'center'}}
            />
            <Text style={{fontSize: 16, color: '#fc913a', fontWeight: 'bold'}}>
              About
            </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flex: 0.5,
            flexDirection: 'column',
            backgroundColor: '#FFF',
            borderRadius: 10,
            margin: 3,
            padding: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              padding: 10,
            }}>
            <Telescope
              name="telescope"
              size={25}
              color="#1e9143"
              style={{marginBottom: 5,alignSelf: 'center'}}
            />
            <Text style={{fontSize: 16,textAlign: 'center', color: '#fc913a', fontWeight: 'bold'}}>
              Vision
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          padding: 15,
          flexDirection: 'row',
        }}>
        <View
          style={{
            flex: 0.5,
            flexDirection: 'column',
            backgroundColor: '#FFF',
            borderRadius: 10,
            margin: 3,
            padding: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              padding: 10,
            }}>
            <Target
              name="target"
              size={25}
              color="#1e9143"
              style={{marginBottom: 5,alignSelf: 'center'}}
            />
            <Text style={{fontSize: 16, color: '#fc913a', fontWeight: 'bold',textAlign: 'center'}}>
              Mission
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 0.5,
            flexDirection: 'column',
            backgroundColor: '#FFF',
            borderRadius: 10,
            margin: 3,
            padding: 10,
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              padding: 10,
            }}>
            <Peoples
              name="people"
              size={25}
              color="#1e9143"
              style={{marginBottom: 5,alignSelf: 'center'}}
            />
            <Text style={{fontSize: 16, color: '#fc913a', fontWeight: 'bold',textAlign: 'center'}}>
              Executive
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          padding: 15,
          flexDirection: 'row',
        }}>
        <View
          style={{
            flex: 0.5,
            backgroundColor: '#FFF',
            borderRadius: 10,
            padding: 5,
            margin:5,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              padding: 10,
            }}>
            <EventNote
              name="event-note"
              size={25}
              color="#1e9143"
              style={{marginBottom: 5,alignSelf: 'center'}}
            />
            <Text style={{fontSize: 16, color: '#fc913a', fontWeight: 'bold',textAlign:'center',}}>
              Upcoming Events
            </Text>
          </View>
        </View>
      </View>
    </>
  );
}
