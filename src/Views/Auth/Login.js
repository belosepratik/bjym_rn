import React, {useState} from 'react';
import {
  KeyboardAvoidingView,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import Input from '../../Components/Basic/Input';
import Wrapped from '../../Components/Global/Wrapped';
import Fontisto from 'react-native-vector-icons/Fontisto';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useDispatch} from 'react-redux';
import {setIsLogged, setUser} from '../../Redux/Slice/LoginSlice';
import {useIsFocused} from '@react-navigation/native';
import ApiCalls from '../../Api/Api';
import {ActivityIndicator} from 'react-native-paper';
import {Config} from '../../../Config';
import Ionicons from 'react-native-vector-icons/Ionicons';
const call = new ApiCalls();
export default function Login({navigation, route}) {
  console.log(route);
  const dispatch = useDispatch();
  const [loading, setloading] = useState(false);
  const [passerr, setpasserr] = useState('Password is required');
  const [phoneerr, setphoneerr] = useState('Phone Number is required');
  const [valid, setvalid] = useState(false);
  const [state, setstate] = useState({
    email: '',
    password: '',
  });
  let res = [];

  const Validate = (e, msg) => {
    // console.log(e, msg)
    const regexEmail = new RegExp(
      /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/,
    );
    const regexPhone = new RegExp(
      /^[+]?(\d{1,2})?[\s.-]?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
    );
    switch (msg) {
      case 'pass':
        if (e.length === 0) {
          setpasserr('Password is required ');
        } else if (e.length <= 5) {
          setpasserr("'Password is too small");
        } else {
          setpasserr();
        }
        break;
      case 'phone':
        if (e.length === 0) {
          setphoneerr('Username is required ');
        } else {
          setphoneerr();
        }
        break;
    }
  };

  const HandleLogin = async () => {
    setvalid(true);
    try {
      if (passerr == null && phoneerr == null) {
        // setloading(true)
        // console.log(state)

        // const data = {
        //   "username": state.email,
        //   "password": `${state.password}`,
        // }
        // res = await call.Calls('user', 'POST', data, Config)
        // if (res.status == '200') {
        //   console.log(res)
        //   dispatch(setUser(res.data))
        //   dispatch(setIsLogged(true))
        //   navigation.replace('Home')
        // } else {
        //   // console.log(res)
        //   console.log(res.msg.response.data.message)
        //   alert(res.msg.response.data.message)
        // }

        dispatch(setIsLogged(true));
        navigation.replace('Home');
      }

      // console.log("res", res,)
      setloading(false);
    } catch (e) {
      console.log(e);
      setloading(false);
    }
  };

  return (
    <Wrapped>
      <ScrollView keyboardShouldPersistTaps="always">
        <View style={{alignItems: 'center', marginTop: 20}}>
          <Image
            source={require('../../../assets/LogoBJYM.jpeg')}
            style={{width: 150, height: 150}}
          />
        </View>

        <View style={{}}>
          <Text
            style={{
              fontSize: 25,
              fontWeight: 'bold',
              alignSelf: 'center',
              marginTop: 20,
            }}>
            Bharatiya Janta Yuva Morcha
          </Text>
        </View>

        <View style={{backgroundColor: 'white', margin: 20}}>
          {/* <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20, marginTop: 20 }}>Login</Text> */}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <Ionicons
              name="call-outline"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Username *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'phone'), setstate({email: e});
              }}
            />
          </View>
          {valid && phoneerr && (
            <Text style={{marginLeft: 30, color: 'red'}}>{phoneerr}</Text>
          )}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <AntDesign
              name="lock"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Password *"
              secureTextEntry={true}
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'pass'), setstate({...state, password: e});
              }}
            />
          </View>
          {valid && passerr && (
            <Text style={{marginLeft: 30, color: 'red'}}>{passerr}</Text>
          )}
          <TouchableOpacity
            onPress={() => HandleLogin()}
            style={{
              margin: 20,
              padding: 10,
              backgroundColor: '#f06b32',
              borderRadius: 10,
            }}>
            {loading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text style={{alignSelf: 'center', fontSize: 20}}>LOGIN</Text>
            )}
          </TouchableOpacity>
          <TouchableOpacity style={{margin: 10}}>
            <Text style={{alignSelf: 'center', fontSize: 20}}>
              Forgot Password?
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{margin: 10}}
            onPress={() => navigation.navigate('Register')}>
            <Text style={{alignSelf: 'center', fontSize: 20}}>
              Don't have an acount? Create here
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Wrapped>
  );
}
