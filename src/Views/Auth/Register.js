import React, {useState} from 'react';
import {
  KeyboardAvoidingView,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import Input from '../../Components/Basic/Input';
import Wrapped from '../../Components/Global/Wrapped';
import Fontisto from 'react-native-vector-icons/Fontisto';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ApiCalls from '../../Api/Api';
import {Config} from '../../../Config';
import {ActivityIndicator} from 'react-native-paper';
import {setIsLogged, setUser} from '../../Redux/Slice/LoginSlice';
import {useDispatch} from 'react-redux';
const call = new ApiCalls();

export default function Register({navigation, route}) {
  const [loading, setloading] = useState(false);
  const [msg, setmsg] = useState('');

  const [name, setname] = useState('Name is required');
  const [emailerr, setemailerr] = useState('Email is required');
  const [passerr, setpasserr] = useState('Password is required');
  const [phoneerr, setphoneerr] = useState('Phone Number is required');
  const [usererr, setusererr] = useState('Username is required');
  const [valid, setvalid] = useState(false);
  const [state, setstate] = useState({
    username: '',
    email: '',
    name: '',
    password: '',
    phone: '',
  });

  const dispatch = useDispatch();

  let res = [];
  const HandleRegister = async () => {
    setvalid(true);
    try {
      setloading(true);

      if (
        name == null &&
        emailerr == null &&
        passerr == null &&
        phoneerr == null
      ) {
        const data = {
          username: state.username,
          email: state.email.toLowerCase(),
          name: state.name.toLowerCase(),
          password: `${state.password}`,
          mobile: state.phone,
          roles: 'admin',
        };

        res = await call.Calls('admin/user', 'POST', data, Config);
        if (res.status == '200') {
          // console.log(res)
          dispatch(setUser(res.data));
          dispatch(setIsLogged(true));
          navigation.replace('Home');
          // navigation.replace(route.params.screen)
        } else {
          // console.log(res)
          console.log(res.msg.response.data.message);
          alert(res.msg.response.data.message);
        }
      }
      console.log(state);

      console.log('res', res);
      setloading(false);
    } catch (e) {
      console.log(e);
      setloading(false);
    }
  };

  const Validate = (e, msg) => {
    // console.log(e, msg)
    const regexname = new RegExp(/^[a-zA-Z ]+$/);
    const regexEmail = new RegExp(
      /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/,
    );
    const regexPhone = new RegExp(
      /^[+]?(\d{1,2})?[\s.-]?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
    );
    switch (msg) {
      case 'name':
        if (e.length === 0) {
          setname('Name is required');
        } else if (!regexname.test(e)) {
          setname('Invalid first name');
        } else {
          setname();
        }
        break;
      case 'username':
        if (e.length === 0) {
          setusererr('username is required ');
        } else {
          setusererr();
        }
        break;
      case 'email':
        if (e.length === 0) {
          setemailerr('Email is required');
        } else if (!regexEmail.test(e)) {
          setemailerr('Invalid email');
        } else {
          setemailerr();
        }
        break;
      case 'pass':
        if (e.length === 0) {
          setpasserr('Password is required ');
        } else if (e.length <= 5) {
          setpasserr("'Password is too small");
        } else {
          setpasserr();
        }
        break;
      case 'phone':
        if (e.length === 0) {
          setphoneerr('Phone Number is required ');
        } else if (!regexPhone.test(e)) {
          setphoneerr('Invalid phone number');
        } else {
          setphoneerr();
        }
        break;
    }
  };

  return (
    <Wrapped>
      <ScrollView keyboardShouldPersistTaps="always">
        <View style={{alignItems: 'center', marginTop: 20}}>
          <Image
            source={require('../../../assets/LogoBJYM.jpeg')}
            style={{width: 200, height: 200}}
          />
        </View>

        <View style={{}}>
          <Text
            style={{
              fontSize: 25,
              fontWeight: 'bold',
              alignSelf: 'center',
              marginTop: 20,
            }}>
            Bharatiya Janta Yuva Morcha
          </Text>
        </View>

        <View style={{backgroundColor: '#F6F0F0 ', margin: 20}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <AntDesign
              name="user"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Name *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'name'), setstate({name: e});
              }}
            />
          </View>
          {valid && name && (
            <Text style={{marginLeft: 30, color: 'red'}}>{name}</Text>
          )}
          {/* <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20, marginTop: 20 }}>Login</Text> */}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <AntDesign
              name="user"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="UserName *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'username'), setstate({...state, username: e});
              }}
            />
          </View>
          {valid && usererr && (
            <Text style={{marginLeft: 30, color: 'red'}}>{usererr}</Text>
          )}

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <Fontisto
              name="email"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Email *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'email'), setstate({...state, email: e});
              }}
            />
          </View>

          {valid && emailerr && (
            <Text style={{marginLeft: 30, color: 'red'}}>{emailerr}</Text>
          )}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <AntDesign
              name="lock"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Password *"
              secureTextEntry={true}
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'pass'), setstate({...state, Password: e});
              }}
            />
          </View>
          {valid && passerr && (
            <Text style={{marginLeft: 30, color: 'red'}}>{passerr}</Text>
          )}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <Ionicons
              name="call-outline"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Phone Number *"
              maxLength={10}
              keyboardType="numeric"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(e) => {
                Validate(e, 'phone'), setstate({...state, phone: e});
              }}
            />
          </View>

          {valid && phoneerr && (
            <Text style={{marginLeft: 30, color: 'red'}}>{phoneerr}</Text>
          )}
          <TouchableOpacity
            onPress={() => HandleRegister()}
            style={{
              margin: 20,
              padding: 10,
              backgroundColor: '#f06b32',
              borderRadius: 10,
            }}>
            {loading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text style={{alignSelf: 'center', fontSize: 20}}>REGISTER</Text>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            style={{margin: 0}}
            onPress={() => navigation.goBack()}>
            <Text style={{alignSelf: 'center', fontSize: 20}}>
              if you are alredy register ? Go to Login
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Wrapped>
  );
}
