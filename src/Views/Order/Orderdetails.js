import React, { useState } from 'react'
import { ScrollView, Text, View } from 'react-native'
import { useSelector } from 'react-redux'
import BillCart from '../../Components/AppComponents/CartComponents/BillCart'
import CartPayFixedBottom from '../../Components/AppComponents/CartComponents/CartPayFixedBottom'
import Input from '../../Components/Basic/Input'
import CommonHeader from '../../Components/Global/CommonHeader'
import Wrapped from '../../Components/Global/Wrapped'
export default function Orderdetails({ navigation, route }) {
    const [valid, setvalid] = useState(false)
    const login = useSelector(state => state.login)
    const { isLogged } = login
    const cart = useSelector(state => state.cart)
    const [amount, setAmount] = React.useState(0);
    const { items } = cart

    const [address, setaddress] = useState('Address is required')
    const [pin, setpin] = useState('Postal Code is required ')

    React.useEffect(() => {
        let initialValue = 0
        let sum = items.reduce(function (accumulator, currentValue) {
            return accumulator + currentValue.orderPrice
        }, initialValue)
        setAmount(sum)
    }, [items])

    const HandlePayment = () => {
        setvalid(true)
        if (address ==null && pin ==null) {
            if (isLogged) {
                navigation.navigate('Payment')
            }
            else {
                navigation.navigate('Login', { "screen": route.name })
            }
        }
    }



    const Validate = (e, msg) => {
        // console.log(e, msg)

        switch (msg) {
            case "address":
                if (e.length === 0) {
                    setaddress("Address is required");
                } else {
                    setaddress();
                }
                break;
            case "pin":
                if (e.length === 0) {
                    setpin("Postal Code is required ");
                } else if (e.length <= 4) {
                    setpin("'Postal Code is too small");
                } else {
                    setpin();
                }
                break;

        }
    }

    return (
        <Wrapped>
            <ScrollView keyboardShouldPersistTaps='always'>
                <CommonHeader name="Order details" back={true} navigation={navigation} />

                <View style={{
                    flexDirection: 'row', alignItems: 'center', marginLeft: 10,
                    marginRight: 10, borderWidth: 0.2, borderRadius: 5, marginTop: 20
                }}>
                    <Input placeholder="Enter Address *" style={{ marginLeft: 10, width: '90%' }} onChangeText={e => { Validate(e, 'address'), console.log(e) }} />
                </View>
                {valid && address && <Text style={{ marginLeft: 20, color: 'red' }}>{address}</Text>}
                <View style={{
                    flexDirection: 'row', alignItems: 'center', marginLeft: 10,
                    marginRight: 10, borderWidth: 0.2, borderRadius: 5, marginTop: 20, marginBottom: 0
                }}>
                    <Input placeholder="Enter Postal Code  *" style={{ marginLeft: 10, width: '90%' }} onChangeText={e => { Validate(e, 'pin'), console.log(e) }} />
                </View>
                {valid && pin && <Text style={{ marginLeft: 20, color: 'red' }}>{pin}</Text>}
                <View style={{marginTop:20}}>
                    <BillCart amount={amount} />
                </View>

            </ScrollView>
            <CartPayFixedBottom amount={amount} HandlePayment={HandlePayment} msg="PROCEED TO PAY" />
        </Wrapped>
    )
}
