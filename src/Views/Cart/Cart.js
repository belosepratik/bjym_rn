import React from 'react'
import { ScrollView, Text, View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useSelector } from 'react-redux'
import ApplyCouponCart from '../../Components/AppComponents/CartComponents/ApplyCouponCart'
import BillCart from '../../Components/AppComponents/CartComponents/BillCart'
import CartFood from '../../Components/AppComponents/CartComponents/CartFood'
import CartPayFixedBottom from '../../Components/AppComponents/CartComponents/CartPayFixedBottom'
import FoodQauntityComponent from '../../Components/AppComponents/CartComponents/FoodQauntityComponent'
import OffPriceCart from '../../Components/AppComponents/CartComponents/OffPriceCart'
import SuggestionCart from '../../Components/AppComponents/CartComponents/SuggestionCart'
import FoodTabSectionComponent from '../../Components/AppComponents/HomeComponents/FoodSectionComponents/FoodTabSectionComponent'
import CommonHeader from '../../Components/Global/CommonHeader'
import Wrapped from '../../Components/Global/Wrapped'
import Header from '../../Components/Header'

const FootTab = [
    {
        "name": "Fruit Mix",
        "id": 1,
        "add": false,
        "left": true,
        "dev": "delivery ",
        "price": '18,500'

    },
    {
        "name": "Fruit Mix",
        "id": 2,
        "add": false,
        "left": false,
        "dev": "delivery discount up to 3x",
        "price": '22,500'
    },
    {
        "name": "Fruit Mix",
        "id": 3,
        "add": false,
        "left": false,
        "dev": "delivery ",
        "price": '18,500'
    },
    {
        "name": "Fruit Mix",
        "id": 4,
        "add": false,
        "left": false,
        "dev": "delivery discount up to 3x",
        "price": '22,500'
    },
]
export default function Cart({ navigation, route }) {
    // const {name ,address}=route.params
    console.log(route)
    const login = useSelector(state => state.login)
    const { isLogged } = login
    const cart = useSelector(state => state.cart)
    const { items } = cart
    const [amount, setAmount] = React.useState(0);
    React.useEffect(() => {
        let initialValue = 0
        let sum = items.reduce(function (accumulator, currentValue) {
            return accumulator + currentValue.orderPrice
        }, initialValue)
        setAmount(sum)
    }, [items])

    const HandlePayment = () => {
        navigation.navigate('Orderdetails', { "amount": amount })
    }

    return (
        <Wrapped>
            {
                items.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 20 }}>Cart is Empty !!!</Text>
                    </View>
                    :
                    <>
                        <ScrollView>
                            <CommonHeader
                                name={typeof route.params != 'undefined' ? route.params.name : "Sindhudurga Kinara"}
                                back={false} navigation={navigation} />
                            {/* <FoodTabSectionComponent  header={false} FootTab={FootTab} /> */}
                            <CartFood name={typeof route.params != 'undefined' ? route.params.name : "Sindhudurga Kinara"}
                                address={typeof route.params != 'undefined' ? route.params.address : "Maharashtra"} />
                            <OffPriceCart />
                            <FoodQauntityComponent />
                            <SuggestionCart />
                            <ApplyCouponCart />
                            <BillCart amount={amount} />
                        </ScrollView>
                        {amount != 0 &&
                            <CartPayFixedBottom amount={amount} HandlePayment={HandlePayment} route={route.name} msg="Checkout" />
                        }

                    </>
            }

        </Wrapped>
    )
}
