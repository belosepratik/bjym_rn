import React, {useState} from 'react';

import {
  Alert,
  TextInput,
  ScrollView,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';
import SettingBox from '../../Components/AppComponents/ProfileComponents/SettingBox';
import Wrapped from '../../Components/Global/Wrapped';
import {useDispatch} from 'react-redux';
import {setIsLogged, setUser} from '../../Redux/Slice/LoginSlice';
import {windowHeight, windowWidth} from '../../Util/Dimensions';

export default function Profile(props) {
  const {navigation} = props;
  const dispatch = useDispatch();
  const handleLogout = () => {
    dispatch(setIsLogged(false));
    navigation.replace('Login');
  };
  return (
    <Wrapped>
      <SafeAreaView>
        <ScrollView>
          <View>
            <View style={(styles.container, {padding: 20, paddingTop: 0})}>
              <Image
                style={styles.bgimg}
                source={require('../../../assets/bjpbg.jpg')}
              />
              <Image
                style={styles.dp}
                source={require('../../../assets/circleBJYM.png')}
              />
            </View>
            <View
              style={{
                marginLeft: 25,
                alignContent: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.name}>Bharatiya Janata Yuva Morcha</Text>
              <Text>Mangalam Shraddha 5th Floor, Model Mile Chowk, Nagpur</Text>
              {/* <Text>info@exits.in</Text> */}
            </View>
            <View
              style={{marginHorizontal: 40, marginBottom: 15, marginTop: 30}}>
              {/* <Text style={{fontSize: 24, fontWeight: 'bold'}}>Settings</Text> */}
            </View>
          {/* <TouchableOpacity onPress={() => navigation.navigate('ChangePasswaord')}>
            <SettingBox iconName="security" title="Change Password" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('ChangeContact')}>
            <SettingBox iconName="database" title="Change Contact" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('EditProfile')}>
            <SettingBox iconName="cog" title="Edit Profile" />
          </TouchableOpacity> */}
          </View>
        </ScrollView>
      </SafeAreaView>
      <View style={{flexDirection:'column',alignItems: 'center'}}>
          <Text style={{textAlign:'center',marginTop:390}}>Developed by Anto & Team ECS [www.exits.in]</Text>
          </View>
    </Wrapped>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bgimg: {
    marginTop: 50,
    marginLeft: -16,
    alignContent: 'center',
    justifyContent: 'center',
    width: windowWidth - 10,
    height: 100,
    borderRadius: 20,
  },
  dp: {
    marginTop: -27,
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 25,
    color:'#F97D09',
    alignContent: 'center',
    justifyContent: 'center',
  },
  logout: {
    flex: 1,
    alignSelf: 'flex-end',

    padding: 25,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'stretch',
    textTransform: 'uppercase',
  },
});
