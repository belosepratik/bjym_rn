import {createSlice} from '@reduxjs/toolkit';
import axios from 'axios';


export const cartslice = createSlice({
  name: 'cart',
  initialState: {
    items:[],

  },
  reducers: {
    setitems:(state, action) => {
      state.items = action.payload;
    },
   
  },
});

//actions
export const {setitems} = cartslice.actions;


export default cartslice.reducer;
