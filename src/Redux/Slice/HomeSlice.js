import {createSlice} from '@reduxjs/toolkit';
import axios from 'axios';


export const homeslice = createSlice({
  name: 'home',
  initialState: {
    loading:false,
    restaurants:[],
    categories:[],
    foods:[]
  },
  reducers: {
    setloading: (state, action) => {
      state.loading = action.payload;
    },
    setrestaurants: (state, action) => {
      state.restaurants = action.payload;
    },
    setcategories: (state, action) => {
      state.categories = action.payload;
    },
    setfoods: (state, action) => {
      state.foods = action.payload;
    },

   
  },
});

//actions
export const {setloading,setrestaurants,setcategories,setfoods} = homeslice.actions;


export default homeslice.reducer;
