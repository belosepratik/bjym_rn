import * as React from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {
  createBottomTabNavigator,
  BottomTabBar,
} from '@react-navigation/bottom-tabs';
import HomeStack from '../Stacks/HomeStacks/HomeStack';
import Icon from 'react-native-vector-icons/AntDesign';
import Profile from '../../Views/ProfileScreen/Profile';
import ProfileStacks from '../Stacks/ProfileStacks/ProfileStacks';
import InfoStack from '../Stacks/InfoStack/InfoStack';
import CartStacks from '../Stacks/CartStacks/CartStacks';
import SearchStacks from '../Stacks/SearchStack/SearchStacks';
import {useSelector} from 'react-redux';
import Svg, {Path} from 'react-native-svg';
const Tab = createBottomTabNavigator();

export default function BottomTabs({}) {
  return (
    <Tab.Navigator
      initialRouteName="HomeStack"
      tabBarOptions={{
        keyboardHidesTabBar: true,
        showLabel: false,
        activeTintColor: '#F97D09',
      }}>
      <Tab.Screen
        name="HomeStack"
        component={HomeStack}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="home" color={color} size={size} />
          ),
          // tabBarBadge: 3,
        }}
      />
        <Tab.Screen
        name="info"
        component={InfoStack}
        options={{
          tabBarIcon: ({color, size}) => (
            // <Icon name="infocirlceo" color={color} size={size} />
            <Image 
            style={{width:75, height:75,position:'relative',top:-14}}
            source={require('../../../assets/circleBJYM.png')}/>
          ),
          // tabBarBadge: 3,
        }}
      />
      {/* <Tab.Screen name="SearchStack" component={SearchStacks}
                options={{
                    tabBarIcon: ({color, size}) => (
                      <HomeIcon fill={color} width={30} height={30} />
                    ),
                    // tabBarBadge: 3,
                  }}
                   
                 /> */}
      {/* <Tab.Screen name="UserForm" component={CartStacks}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        // <View style={{
                        //     width: 55,
                        //     height: 55,
                        //     backgroundColor: 'white',
                        //     borderRadius: 30,
                        //     justifyContent: 'center',
                        //     alignItems: 'center',
                        //     marginBottom: Platform.OS == "android" ? 50 : 30
                        //   }}>
                            <Icon name="pluscircleo" color={color} size={size} />
                        // </View>

                    ),
                }} /> */}
      {/* <Tab.Screen name="CartStacks" component={CartStacks}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <Image
                            source={require("../../../assets/icons/like.png")}
                            resizeMode="contain"
                            style={{
                                width: 25,
                                height: 25,
                                marginBottom: !focused ? 10 : 0,
                                tintColor: focused ? '#f06b32' : "#CDCDD2",
                            }}
                        />
                    ),
                    tabBarBadge: items.length != 0 ? items.length : null,
                    tabBarBadgeStyle: { marginLeft: buton ? 0 : 50 },
                    tabBarButton: (props) => (
                        <TabBarCustomButton HandleButon={HandleButon} value={true}
                            {...props}
                        />
                    )
                }} /> */}
              
      <Tab.Screen
        name="ProfileStacks"
        component={ProfileStacks}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="user" color={color} size={size} />
          ),
          // tabBarBadge: 3,
        }}
      />
    </Tab.Navigator>
  );
}
