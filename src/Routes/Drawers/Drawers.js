import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import BottomTabs from '../BottomTabs/BottomTabs';
import DrawerContent from './DrawerContent';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import Register from '../../Views/Auth/Register';
import Login from '../../Views/Auth/Login';
import Category from '../../Views/HomeScreens/Category/Category';
import Restaurents from '../../Views/HomeScreens/Category/Restaurents';
import Cart from '../../Views/Cart/Cart';
import Payment from '../../Views/Payment.js/Payment';
import Order from '../../Views/Order/Order';
import Orderdetails from '../../Views/Order/Orderdetails';
import Categorydtails from '../../Views/HomeScreens/Categorydtails';
import Resturentdetails from '../../Views/HomeScreens/Resturentdetails';
import FoodSectionScreen from '../../Views/HomeScreens/FoodSectionScreen';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
export default function Drawers(props) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        //  gestureEnabled: true,
        // gestureDirection: 'horizontal',
        // cardStyleInterpolator:
        //   CardStyleInterpolators.forHorizontalIOS
      }}
      initialRouteName="BottomTabs">
      <Stack.Screen name="BottomTabs" component={BottomTabs} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Category" component={Category} />
      <Stack.Screen name="Restaurents" component={Restaurents} />
      <Stack.Screen name="Cart" component={Cart} />
      <Stack.Screen name="Order" component={Order} />
      <Stack.Screen name="Categorydtails" component={Categorydtails} />
      <Stack.Screen name="Resturentdetails" component={Resturentdetails} />
      <Stack.Screen
        name="FoodSectionScreenwtab"
        component={FoodSectionScreen}
      />
      <Stack.Screen name="Orderdetails" component={Orderdetails} />
      <Stack.Screen name="Payment" component={Payment} />
    </Stack.Navigator>
    // <Drawer.Navigator initialRouteName="UserActivity" openByDefault={false}
    //   drawerContent={props=> <DrawerContent {...props}/>} drawerStyle={{
    //     backgroundColor: 'white',
    //     width: 220,}}>
    //     <Drawer.Screen name="UserActivity" component={BottomTabs} />
    //   </Drawer.Navigator>
  );
}
