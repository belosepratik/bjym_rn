import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Drawers from './Drawers/Drawers';
import AuthStacks from './Stacks/AuthStacks/AuthStacks';
import {useSelector} from 'react-redux';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from '../Views/SplashScreen';
import Login from '../Views/Auth/Login';
import Register from '../Views/Auth/Register';
import ChangePassword from './ChangePassword/ChangePassword';
import Profile from '../Views/ProfileScreen/Profile';
import ChangeContact from './ChangeContact/ChangeContact';
import EditProfile from './EditProfile/EditProfile';
import ProfileStack from './Stacks/ProfileStacks/ProfileStacks';
import About from '../Views/About/About';
import InfoScreen from '../Views/InfoScreen/InfoScreen';
import InfoStack from './Stacks/InfoStack/InfoStack';
const Stack = createStackNavigator();
export default function Navigation() {
  const login = useSelector((state) => state.login);
  const {isLogged} = login;
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="SplashScreen">
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Home" component={Drawers} />
        <Stack.Screen name="ChangePasswaord" component={ChangePassword} />
        <Stack.Screen name="ProfileStack" component={ProfileStack} />
        <Stack.Screen name="ChangeContact" component={ChangeContact} />
        <Stack.Screen name="EditProfile" component={EditProfile} />
        <Stack.Screen name="About" component={About} />
        <Stack.Screen name="InfoStack" component={InfoStack} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
