import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import Profile from '../../../Views/ProfileScreen/Profile';
import Login from '../../../Views/Auth/Login';
import Register from '../../../Views/Auth/Register';

const Stack = createStackNavigator();
export default function ProfileStacks() {
  return (
    // define here all Home tabs route here
    <Stack.Navigator initialRouteName="ProfileScreen" screenOptions={{ headerShown: false }}>
      <Stack.Screen name="ProfileScreen" component={Profile} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
    </Stack.Navigator>
  )
}
