import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import Cart from '../../../Views/Cart/Cart';
const Stack = createStackNavigator();
export default function CartStacks() {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="CartStack" component={Cart} />

        </Stack.Navigator>
    )
}
