import React from 'react'

import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack';
import Home from '../../../Views/HomeScreens/Home';
import FoodSectionScreen from '../../../Views/HomeScreens/FoodSectionScreen';

const Stack = createStackNavigator();
export default function HomeStack() {
  return (
    // define here all Home tabs route here
    <Stack.Navigator initialRouteName="HomeScreen" screenOptions={{
      headerShown: false, gestureEnabled: true,
      gestureDirection: 'horizontal',
      cardStyleInterpolator:
        CardStyleInterpolators.forHorizontalIOS
    }}>
      <Stack.Screen name="HomeScreen" component={Home} />
      <Stack.Screen name="FoodSectionScreen" component={FoodSectionScreen} />
    </Stack.Navigator>
  )
}
