import React from 'react'
import {createStackNavigator} from '@react-navigation/stack';
import Search from '../../../Views/Search/Search';
const Stack = createStackNavigator();
export default function SearchStacks() {
    return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Search" component={Search} /> 
      </Stack.Navigator>
    )
}
