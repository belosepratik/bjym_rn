import React from 'react'

import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack';
import InfoScreen from '../../../Views/InfoScreen/InfoScreen';
import FoodSectionScreen from '../../../Views/HomeScreens/FoodSectionScreen';
import Profile from '../../../Views/ProfileScreen/Profile';
import About from '../../../Views/About/About';

const Stack = createStackNavigator();
export default function InfoStack() {
  return (
    // define here all Home tabs route here
    <Stack.Navigator initialRouteName="InfoScreen" screenOptions={{
      headerShown: false, gestureEnabled: true,
      gestureDirection: 'horizontal',
      cardStyleInterpolator:
        CardStyleInterpolators.forHorizontalIOS
    }}>
      {/* <Stack.Screen name="InfoScreen" component={InfoScreen} /> */}
      <Stack.Screen name="About" component={About} /> 
      <Stack.Screen name="FoodSectionScreen" component={FoodSectionScreen} />
    </Stack.Navigator>
  )
}
