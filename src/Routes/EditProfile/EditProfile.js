import React, { useState } from "react";
import { Alert, Modal, StyleSheet, Text,
     Pressable, View,TextInput,Image,
     TouchableOpacity,ScrollView,SafeAreaView } from "react-native";

     import { Icon } from 'react-native-elements'

const EditProfile = ({navigation}) => {
  
  return (
  <SafeAreaView>
    <ScrollView >
        <View style={{padding:20,justifyContent: 'space-between', flexDirection: 'column'}}>
            <View style={{alignItems: 'flex-start'}}>
            <TouchableOpacity onPress={() => navigation.navigate('ProfileStacks')}><Icon
            name='arrow-back' 
              size={40}
            /></TouchableOpacity>
            
            </View>
            <View style={{alignItems:'center',marginTop: 20}}>
            <Image
            source={require('../../../assets/circleBJYM.png')}
            style={{width: 240, height: 240}}
          />
            </View>
            <View style={{marginTop:25}}>
                <Text style={{textAlign: 'center',fontSize: 20,fontWeight: 'bold'}}>Edit Profile</Text>
            </View>
            <View style={{marginTop:20}}>
            <TextInput
          style={{
            color:'#fc913a',
            marginBottom: 30,
            borderColor: '#fc913a',
            borderWidth: 1,
            borderRadius:15,
          }}
          placeholder="Change Name" placeholderTextColor="#fc913a"
        />
         <TextInput
          style={{
            color:'#fc913a',
            marginBottom: 30,
            borderColor: '#fc913a',
            borderWidth: 1,
            borderRadius:15,
          }}
          placeholder="Change Date Of Birth" placeholderTextColor="#fc913a"
        />
         <TextInput
          style={{
            color:'#fc913a',
            marginBottom: 30,
            borderColor: '#fc913a',
            borderWidth: 1,
            borderRadius:15,
          }}
          placeholder="Change Email Address" placeholderTextColor="#fc913a"
        />
           <TextInput
          style={{
            color:'#fc913a',
            marginBottom: 30,
            borderColor: '#fc913a',
            borderWidth: 1,
            borderRadius:15,
          }}
          placeholder="Change Address" placeholderTextColor="#fc913a"
        />
            </View>
            <View>
                <TouchableOpacity style={{backgroundColor: '#1e9143',borderRadius:10,height:'22%',justifyContent:'center'}}>
                    <Text style={{textAlign:'center',fontSize:20,fontWeight: '800',color:'white' }}>Change Profile</Text>
                </TouchableOpacity>
            </View>
        </View>
    </ScrollView>
  </SafeAreaView>  
  );
};

export default EditProfile;


