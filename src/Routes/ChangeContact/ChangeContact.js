import React, { useState } from "react";
import { Alert, Modal, StyleSheet, Text,
     Pressable, View,TextInput,Image,
     TouchableOpacity,ScrollView,SafeAreaView } from "react-native";

     import { Icon } from 'react-native-elements'

const ChangeContact = ({navigation}) => {
  
  return (
  <SafeAreaView>
    <ScrollView >
        <View style={{padding:20,justifyContent: 'space-between', flexDirection: 'column'}}>
            <View style={{alignItems: 'flex-start'}}>
            <TouchableOpacity onPress={() => navigation.navigate('ProfileStacks')}><Icon
            name='arrow-back' 
              size={40}
            /></TouchableOpacity>
            
            </View>
            <View style={{alignItems:'center',marginTop: 20}}>
            <Image
            source={require('../../../assets/circleBJYM.png')}
            style={{width: 240, height: 240}}
          />
            </View>
            <View style={{marginTop:25}}>
                <Text style={{textAlign: 'center',fontSize: 20,fontWeight: 'bold'}}>Change Contact</Text>
            </View>
            <View style={{marginTop:20}}>
            <TextInput
            keyboardType="numeric"
          style={{
            color:'#fc913a',
            marginBottom: 30,
            borderColor: '#fc913a',
            borderWidth: 1,
            borderRadius:15,
          }}
          placeholder="Enter Old Contact" placeholderTextColor="#fc913a"
        />
         <TextInput
         keyboardType="numeric"
          style={{
            color:'#fc913a',
            marginBottom: 30,
            borderColor: '#fc913a',
            borderWidth: 1,
            borderRadius:15,
          }}
          placeholder="Enter New Contact" placeholderTextColor="#fc913a"
        />
         <TextInput
         keyboardType="numeric"
          style={{
            color:'#fc913a',
            marginBottom: 30,
            borderColor: '#fc913a',
            borderWidth: 1,
            borderRadius:15,
          }}
          placeholder="Confirm Contact" placeholderTextColor="#fc913a"
        />
            </View>
            <View>
                <TouchableOpacity style={{backgroundColor: '#1e9143',borderRadius:10,height:'24%',justifyContent:'center'}}>
                    <Text style={{textAlign:'center',fontSize:20,fontWeight: 'bold',color:'white' }}>Change Contact</Text>
                </TouchableOpacity>
            </View>
        </View>
    </ScrollView>
  </SafeAreaView>  
  );
};

export default ChangeContact;


