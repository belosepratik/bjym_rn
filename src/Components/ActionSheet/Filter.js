import React, { useState } from 'react'
import { Text, View } from 'react-native'
import { RadioButton } from 'react-native-paper';

export default function Filter() {
    const [value, setValue] = React.useState('first');
    const [select, setselect] = useState(0)
    return (
        <View style={{
            backgroundColor: 'white',
            borderTopLeftRadius: 12,
            borderTopRightRadius: 12,
            height: '100%'
        }}>

            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Sort / Filter</Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                <View style={{ flex: 0.4, borderRightWidth: 0.4 }}>
                    <View style={{ justifyContent: 'center' }}>
                        <View style={{ borderWidth: 0.4 }} />
                        <Text onPress={() => setselect(0)} style={{ fontSize: 20, fontWeight: 'bold', margin: 10, color: select == 0 ? '#f06b32' : 'black' }}>Sort</Text>
                        <View style={{ borderWidth: 0.4, marginTop: 5 }} />
                        <Text onPress={() => setselect(1)} style={{ fontSize: 20, fontWeight: 'bold', margin: 10, color: select == 1 ? '#f06b32' : 'black' }}>Veg</Text>
                        <View style={{ borderWidth: 0.4, marginTop: 5 }} />
                        <Text onPress={() => setselect(2)} style={{ fontSize: 20, fontWeight: 'bold', margin: 10, color: select == 2 ? '#f06b32' : 'black' }}>Offer & More</Text>
                        <View style={{ borderWidth: 0.4, marginTop: 5 }} />
                    </View>


                </View>
                <View style={{ flex: 0.6, }}>
                    <View style={{ borderWidth: 0.5 }} />
                    <Text style={{ fontSize: 16, color: 'grey', margin: 10 }}>Sort</Text>

                    {
                        select == 0 ?
                            <View style={{ marginLeft: 10 }}>

                                {/* <RadioButton.Group  onValueChange={newValue => setValue(newValue)} value={value}> */}
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <RadioButton
                                        uncheckedColor='#E0E0E0' color='#f06b32'
                                        value="first"
                                        status={'checked'}
                                    // onPress={() => { setChecked('first'), setmsg('Changed my plans') }}
                                    />
                                    <Text style={{ fontSize: 18 }}>First</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <RadioButton
                                        uncheckedColor='#E0E0E0' color='#f06b32'
                                        value="first"
                                        status={'checked'}
                                    // onPress={() => { setChecked('first'), setmsg('Changed my plans') }}
                                    />
                                    <Text style={{ fontSize: 18 }}>second</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <RadioButton
                                        uncheckedColor='#E0E0E0' color='#f06b32'
                                        value="first"
                                        status={'checked'}
                                    // onPress={() => { setChecked('first'), setmsg('Changed my plans') }}
                                    />
                                    <Text style={{ fontSize: 18 }}>third</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <RadioButton
                                        uncheckedColor='#E0E0E0' color='#f06b32'
                                        value="first"
                                        status={'unchecked'}
                                    // onPress={() => { setChecked('first'), setmsg('Changed my plans') }}
                                    />
                                    <Text style={{ fontSize: 18 }}>fourth</Text>
                                </View>
                                {/* </RadioButton.Group> */}
                            </View>
                            : <>{
                                select == 1 ?
                                    <View style={{ marginLeft: 10 }}>

                                        {/* <RadioButton.Group  onValueChange={newValue => setValue(newValue)} value={value}> */}
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <RadioButton
                                                uncheckedColor='#E0E0E0' color='#f06b32'
                                                value="first"
                                                status={'checked'}
                                            // onPress={() => { setChecked('first'), setmsg('Changed my plans') }}
                                            />
                                            <Text style={{ fontSize: 18 }}>First</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <RadioButton
                                                uncheckedColor='#E0E0E0' color='#f06b32'
                                                value="first"
                                                status={'checked'}
                                            // onPress={() => { setChecked('first'), setmsg('Changed my plans') }}
                                            />
                                            <Text style={{ fontSize: 18 }}>second</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <RadioButton
                                                uncheckedColor='#E0E0E0' color='#f06b32'
                                                value="first"
                                                status={'checked'}
                                            // onPress={() => { setChecked('first'), setmsg('Changed my plans') }}
                                            />
                                            <Text style={{ fontSize: 18 }}>third</Text>
                                        </View>

                                        {/* </RadioButton.Group> */}
                                    </View>
                                    :
                                    <View style={{ marginLeft: 10 }}>

                                        {/* <RadioButton.Group  onValueChange={newValue => setValue(newValue)} value={value}> */}
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <RadioButton
                                                uncheckedColor='#E0E0E0' color='#f06b32'
                                                value="first"
                                                status={'checked'}
                                            // onPress={() => { setChecked('first'), setmsg('Changed my plans') }}
                                            />
                                            <Text style={{ fontSize: 18 }}>First</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <RadioButton
                                                uncheckedColor='#E0E0E0' color='#f06b32'
                                                value="first"
                                                status={'checked'}
                                            // onPress={() => { setChecked('first'), setmsg('Changed my plans') }}
                                            />
                                            <Text style={{ fontSize: 18 }}>second</Text>
                                        </View>

                                        {/* </RadioButton.Group> */}
                                    </View>
                            }</>
                    }



                </View>

            </View>



        </View>
    )
}
