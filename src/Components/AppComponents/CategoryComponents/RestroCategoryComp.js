import React, { useRef, useState } from 'react'
import { View, Text } from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useSelector } from 'react-redux'
import Skeleton from '../../Skeleton/Skeleton'
import ActionSheet from 'react-native-actions-sheet';
import FoodTabSectionComponent from '../HomeComponents/FoodSectionComponents/FoodTabSectionComponent'
import Filter from '../../ActionSheet/Filter'
import { windowHeight } from '../../../Util/Dimensions'
export default function RestroCategoryComp({ HandleRestro, restro, addbutton,navigation }) {

    const actionSheetRef = useRef();
    const FootTab = [
        {
            "name": "Fruit Mix",
            "id": 1,
            "add": true,
            "left": true,
            "dev": "50% OFF upto ₹1000 ",
            "price": '₹18,500',
            "discount": "50% OFF",
            "button": false,
            "top": true,
            "rating": 4.0,
            "health": "Healthy Anterantives available",
            "address": "Azad nagar Mithagar road mulund",
            "city": "thane",
            "distance": "35 kms"

        },
        {
            "name": "Fruit Mix",
            "id": 2,
            "add": true,
            "left": true,
            "dev": "50% OFF upto ₹1000 ",
            "price": '₹22,500',
            "discount": "50% OFF",
            "button": false,
            "top": true,
            "rating": 4.0,
            "health": "Healthy Anterantives available",
            "address": "Azad nagar Mithagar road mulund",
            "city": "Thane",
            "distance": "35 kms"

        },
        {
            "name": "Fruit Mix",
            "id": 3,
            "add": true,
            "left": true,
            "dev": "50% OFF upto ₹1000 ",
            "price": '₹18,500',
            "discount": "50% OFF",
            "button": false,
            "top": true,
            "rating": 4.0,
            "health": "Healthy Anterantives available",
            "address": "Azad nagar Mithagar road mulund mumbai 400081",
            "city": "Thane",
            "distance": "35 kms"
        },
        {
            "name": "Fruit Mix",
            "id": 4,
            "add": true,
            "left": true,
            "dev": "50% OFF upto ₹1000 ",
            "price": '₹22,500',
            "discount": "50% OFF",
            "button": false,
            "top": true,
            "rating": 4.0,
            "health": "Healthy Anterantives available",
            "address": "Azad nagar Mithagar road mulund",
            "city": "Thane",
            "distance": "35 kms"
        },
    ]
    const HandleActionsheet = () => {
        actionSheetRef.current?.setModalVisible();
    }
    return (
        <View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                <Text>
                    4 RESTAURENTS NEARBY
                </Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <AntDesign name="filter" size={20} />
                    <Text onPress={()=>HandleActionsheet()}>
                        SORT / FILTER
                    </Text>
                </View>
            </View>

            <View style={{ marginTop: 20 }}>
                <FoodTabSectionComponent header={false} FootTab={restro} HandleRestro={HandleRestro} addbutton={addbutton} tabs={0} />
            </View>
            <ActionSheet
                ref={actionSheetRef}
                headerAlwaysVisible={true}
                statusBarTranslucent
                containerStyle={{ height: windowHeight/1.5 }}
                bounceOnOpen={true}>
                <Filter
                    // Timeroute={Timeroute}
                    navigation={navigation}
                />
            </ActionSheet>
        </View>
    )
}
