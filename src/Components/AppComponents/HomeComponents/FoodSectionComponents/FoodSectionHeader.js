import React from 'react'
import { Text, View, Image } from 'react-native'
import { windowHeight } from '../../../../Util/Dimensions'
import Ion from 'react-native-vector-icons/Ionicons'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Icon from 'react-native-vector-icons/MaterialIcons'
import HeartIcon from '../../../Global/HeartIcon'
export default function FoodSectionHeader({ img, name,address,navigation }) {
    // const {img,name}=route.params
    const BackButton =()=>{
        navigation.goBack()
    }

    return (
        <View>
            <View style={{ height: windowHeight / 2,backgroundColor:'#F8F8F8' }}>
                <Image source={{ uri: img }} style={{ width: "100%", height: windowHeight / 3, }} />
                <View style={{ marginTop: -windowHeight / 3 }}>
                    <View style={{ justifyContent: 'space-between', flexDirection: 'row', margin: 10,}}>
                    <HeartIcon icon="MaterialCommunityIcons"  name="less-than" size={25}  color='#f06b32' wh={35} navigation={navigation} BackButton={BackButton} />
                    <HeartIcon icon="Ionicons"  name="heart-outline" size={25}  color='#f06b32' wh={35} />
                      
                    </View>
                </View>

                <View style={{ marginTop: -windowHeight / 6, marginBottom: 100, }}>
                    <View style={{ backgroundColor: 'white', margin: 20, marginTop: windowHeight / 3, borderRadius: 10 }}>
                        <Text style={{ fontSize: 25, marginTop: 20, fontWeight: 'bold', alignSelf: 'center' }}>{name}</Text>
                        <Text style={{ fontSize: 18, color: 'grey', alignSelf: 'center', marginTop: 10 }}>{address}</Text>
                        <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 10 }}>
                            <Text >Open</Text>
                            <Text style={{ color: 'grey' }}> 8 am - 8pm</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'center', marginBottom: 20 }}>
                            <View style={{ flexDirection: 'row', }}>
                                <EvilIcons name='location' size={15} color='#f06b32' />
                                <Text style={{ marginLeft: 5 }}>1km</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                                <Ion name="star-outline" size={15} color='#f06b32' />
                                <Text style={{ marginLeft: 5 }}>5.0</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                                <Icon name="verified-user" size={15} color='#f06b32' />
                                <Text style={{ marginLeft: 5 }}>Verified</Text>
                            </View>


                        </View>

                    </View>
                </View>
            </View>

        </View>
    )
}
