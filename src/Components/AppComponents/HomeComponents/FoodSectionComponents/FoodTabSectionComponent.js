import React, { useState } from 'react'
import { View, Text, Image, TouchableHighlight, StyleSheet, FlatList, TouchableOpacity, ScrollView } from 'react-native'
import InputSpinner from 'react-native-input-spinner'
import { windowHeight, windowWidth } from '../../../../Util/Dimensions'
import Icon from 'react-native-vector-icons/Ionicons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import HeartIcon from '../../../Global/HeartIcon'
import FoodTabSectionHeader from './FoodTabSectionHeader'
import Ent from 'react-native-vector-icons/Entypo'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { setitems } from '../../../../Redux/Slice/CartSlice'
import Ion from 'react-native-vector-icons/Ionicons'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import MIcon from 'react-native-vector-icons/MaterialIcons'
import RestorauntDetails from '../../../Global/RestorauntDetails'
import Review from '../../../Global/Review'

export default function FoodTabSectionComponent({ header, HandleTabs, tabs, FootTab, HandleRestro, addbutton, Key, name, address,route }) {
    console.log(route)
    const [count, setcount] = useState(0)
    const dispatch = useDispatch()
    const cart = useSelector(state => state.cart)
    const { items } = cart
    const [parentid, setparentid] = useState([])
    let cancel = [];



    React.useEffect(() => {
        if (items.length !== 0) {
            items.map((d, i) => {
                cancel.push(
                    d.id
                )

            })

            setparentid(cancel)
        } else {

            setparentid([])
        }
    }, [items])

    const HandleData = (item, name, id) => {
        let selects = [...items]

        selects.push({
            "name": name,
            "id": item.id,
            "item": item.item,
            "price": item.price,
            "orderPrice": item.price,
            "item": item.item,
            "quantity": 1

        })
        dispatch(setitems(selects))

    }





    const HandleCancel = (item) => {
        const result = items
            .filter(function (element) {
                return element.id != item.id;
            });
        dispatch(setitems(result))
        // console.log(result)

    }

    return (
        <>
            <View style={styles.container}>
                {

                    header && <FoodTabSectionHeader header={header} HandleTabs={HandleTabs} tabs={tabs} />
                }

            </View>
            {
                tabs == 0 ?
                    <>
                        <FlatList
                            data={FootTab}
                            keyExtractor={(item, index) => index.toString()}
                            contentContainerStyle={{ paddingTop: header ? 20 : -20, backgroundColor: 'white' }}
                            renderItem={({ item }) => (
                                <>
                                    <TouchableOpacity activeOpacity={addbutton ? 1 : 0} style={{ justifyContent: "space-between", flexDirection: 'row', margin: 10, marginTop: item.top && 20 }}
                                        onPress={() => { item.top && HandleRestro(item) }}
                                    >
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image
                                                source={require('../../../../../assets/food.jpeg')}
                                                style={{ height: 100, width: 100, borderRadius: 10 }}
                                            />

                                            <View style={{ marginLeft: 20, marginTop: item.top ? 0 : 10 }}>

                                                <Text style={{ fontSize: 18, fontWeight: 'bold', textTransform: 'capitalize' }}>{item.name}</Text>
                                                {item.address &&

                                                    <Text style={{ color: 'grey', marginTop: 5, }}> {item.address.length < 30
                                                        ? `${item.address}`
                                                        : `${item.address.substring(0, 30)}...`}</Text>


                                                }
                                                {item.city &&

                                                    <Text style={{ color: 'grey', marginTop: 5, }}>{item.city} | {item.distance}</Text>


                                                }
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    {
                                                        item.rating &&
                                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 5 }}>
                                                            <Ent name="star" size={15} color='grey' style={{ marginRight: 5, marginTop: 2 }} />
                                                            <Text style={{ color: 'grey', marginTop: 5, fontSize: 16, alignSelf: 'center' }}>{item.rating}</Text>
                                                        </View>
                                                    }
                                                    <Text style={{ color: 'grey', marginTop: 5 }}>₹{item.price}</Text>
                                                    <Text style={{ color: 'grey', marginTop: 5, marginLeft: 10, textDecorationLine: 'line-through' }}>22,500</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
                                                    <HeartIcon icon="MaterialCommunityIcons" name="percent" size={15} color="white" bg="#f06b32" wh={15} />
                                                    <Text style={{ margin: 4 }}>{item.dev}</Text>
                                                </View>
                                            </View>
                                        </View>
                                        <HeartIcon icon="Ionicons" name="heart-outline" size={25} color="#f06b32" />
                                    </TouchableOpacity>
                                    {
                                        item.left &&
                                        <View style={{
                                            position: 'absolute', marginTop: windowHeight / 8,
                                            left: windowWidth / 11, padding: 7, backgroundColor: '#f06b32',
                                            alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderColor: 'white'
                                        }}>
                                            <Text style={{ color: 'white', }}>{item.discount ? item.discount : "5 left"}</Text>
                                        </View>
                                    }




                                    {
                                        addbutton &&
                                        <>
                                            {!parentid.includes(item.id) ?
                                                <View style={{ justifyContent: 'flex-end', flexDirection: 'row', marginBottom: 10 }}>
                                                    <TouchableOpacity onPress={() => HandleData(item)} style={{ backgroundColor: '#f06b32', marginRight: 20, borderRadius: 10 }}>
                                                        <Text style={{ fontSize: 20, paddingLeft: 15, paddingTop: 5, paddingBottom: 5, paddingRight: 15, color: 'white' }}>+   Add</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                :
                                                <>


                                                    <View style={{ justifyContent: 'flex-end', flexDirection: 'row', marginBottom: 10 }}>
                                                        <TouchableOpacity onPress={() => HandleCancel(item)} style={{ backgroundColor: '#f06b32', marginRight: 20, borderRadius: 10 }}>
                                                            <Text style={{ fontSize: 20, paddingLeft: 15, paddingTop: 5, paddingBottom: 5, paddingRight: 15, color: 'white' }}>Cancel</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </>}
                                        </>
                                    }


                                    {/* <View style={{borderWidth:0.2,marginTop:10}}/> */}

                                </>
                            )}
                        />
                    </>
                    :
                    <>
                        {
                            tabs == 1 ?
                               <ScrollView>
                               <Review/>
                               </ScrollView>
                                :
                                <ScrollView>
                                <RestorauntDetails name={name} address={address}/>
                                </ScrollView>
                               
                        }
                    </>



            }

        </>



    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingTop: 0
    },
    list: {
        flexDirection: 'row',
        width: '100%',
        backgroundColor: 'white',
        //   height: 40,
        justifyContent: 'space-around',
        alignItems: 'center',
        margin: 10,
    },
    activeBorder: {
        borderBottomWidth: 2,
        borderColor: 'orange',
        padding: 5,
        fontSize: 18,
        fontWeight: 'bold'
    },
    txt: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    foodCategory: {
        width: '90%',
        height: 100,
        backgroundColor: 'white',
        flexDirection: 'row',
        marginBottom: 20
    },
    foodContent: {
        width: '80%',
        //margin: 5,
        padding: 5,
        paddingLeft: 20,
        backgroundColor: 'white',
    },
    title: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    likeIcon: {
        height: 30,
        width: 30,
    },
    btn: {
        marginTop: 5,
        height: 40,
        width: 40,
        backgroundColor: '#f06b32',
        justifyContent: 'center',
        paddingLeft: 15,
        borderRadius: 5,
        marginLeft: -1,
        marginRight: -1,
    },
    btnAdd: {
        height: 40,
        width: 100,
        backgroundColor: '#f06b32',
        justifyContent: 'center',
        paddingLeft: 20,
    },
    btnTxt: {
        fontSize: 20,
        color: 'white',
        fontWeight: '700',
    },
    countNo: {
        marginTop: 5,
        height: 40,
        width: 40,
        backgroundColor: 'silver',
        justifyContent: 'center',
        paddingLeft: 15,
    },
    floatBtn: {
        width: '90%',
        height: 50,
        backgroundColor: '#f06b32',
        position: 'absolute',
        top: '90%',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    floatBtnTxt: {
        alignSelf: 'center',
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
    },
})