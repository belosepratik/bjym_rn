import React from 'react'
import { View, Text, Image, TouchableHighlight, StyleSheet, FlatList, TouchableOpacity } from 'react-native'

export default function FoodTabSectionHeader({header,HandleTabs,tabs}) {
    return (
        <View style={styles.list}>
        <Text style={tabs==0 ? styles.activeBorder :styles.txt} onPress={()=>HandleTabs(0)} >Menu</Text>
        <Text style={tabs==1 ? styles.activeBorder :styles.txt}  onPress={()=>HandleTabs(1)}>Reviews</Text>
        <Text style={tabs==2 ? styles.activeBorder :styles.txt}  onPress={()=>HandleTabs(2)}>Info</Text>
    </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingTop: 0
    },
    list: {
        flexDirection: 'row',
        width: '100%',
        backgroundColor: 'white',
        //   height: 40,
        justifyContent: 'space-around',
        alignItems: 'center',
        margin: 10,
    },
    activeBorder: {
        borderBottomWidth: 2,
        borderColor: 'orange',
        padding: 5,
        fontSize: 18,
        fontWeight: 'bold'
    },
    txt: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    foodCategory: {
        width: '90%',
        height: 100,
        backgroundColor: 'white',
        flexDirection: 'row',
        marginBottom: 20
    },
    foodContent: {
        width: '80%',
        //margin: 5,
        padding: 5,
        paddingLeft: 20,
        backgroundColor: 'white',
    },
    title: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    likeIcon: {
        height: 30,
        width: 30,
    },
    btn: {
        marginTop: 5,
        height: 40,
        width: 40,
        backgroundColor: '#f06b32',
        justifyContent: 'center',
        paddingLeft: 15,
        borderRadius: 5,
        marginLeft: -1,
        marginRight: -1,
    },
    btnAdd: {
        height: 40,
        width: 100,
        backgroundColor: '#f06b32',
        justifyContent: 'center',
        paddingLeft: 20,
    },
    btnTxt: {
        fontSize: 20,
        color: 'white',
        fontWeight: '700',
    },
    countNo: {
        marginTop: 5,
        height: 40,
        width: 40,
        backgroundColor: 'silver',
        justifyContent: 'center',
        paddingLeft: 15,
    },
    floatBtn: {
        width: '90%',
        height: 50,
        backgroundColor: '#f06b32',
        position: 'absolute',
        top: '90%',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    floatBtnTxt: {
        alignSelf: 'center',
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
    },
})