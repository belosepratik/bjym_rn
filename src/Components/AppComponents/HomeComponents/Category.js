import React, { useState } from 'react'
import { View, Image, Text, SafeAreaView, StyleSheet, ScrollView, True, FlatList, TouchableOpacity } from 'react-native'

const CategoryData = [
  {
    "name": "Non Veg",
    "icon": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTsnKibjpbmtpKj8QTj-fXv3uZH_jqsRBJow&usqp=CAU",
    "id": 1
  },
  {
    "name": "Vegitables",
    "icon": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRu-CkRGLmZwwLcVZku_t0woWTro6hMcjF4ng&usqp=CAU",
    "id": 2
  },
  {
    "name": "Fish",
    "icon": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTsnKibjpbmtpKj8QTj-fXv3uZH_jqsRBJow&usqp=CAU",
    "id": 3
  },
  {
    "name": "Fruits",
    "icon": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS05yiGXJ1SOZ3bN1knwXM-jFtVnMw9dquhxA&usqp=CAU",
    "id": 4
  },
  {
    "name": "Non Veg",
    "icon": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTsnKibjpbmtpKj8QTj-fXv3uZH_jqsRBJow&usqp=CAU",
    "id": 5
  },
  {
    "name": "Non Veg",
    "icon": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTsnKibjpbmtpKj8QTj-fXv3uZH_jqsRBJow&usqp=CAU",
    "id": 6
  },

]

export default function Category({ data, HandleCategory, navigation }) {
  // console.log(data.results)
  const [Cat, setCat] = useState([])
  let arr = []
  React.useEffect(() => {
    try {
      if (data) {

        data.map((d, i) => {
          // console.log(d, i)
          arr.push({
            name: d.name,
            id: d._id,
            desc: d.desc,
            veg: d.veg == 'true' ? "Veg" : "Non Veg",
            pic: d.cover_pic,
            restros: d.restros,
            icon: i == 0 ? "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTsnKibjpbmtpKj8QTj-fXv3uZH_jqsRBJow&usqp=CAU"
              : i == 1 ? "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS05yiGXJ1SOZ3bN1knwXM-jFtVnMw9dquhxA&usqp=CAU"
                : i == 3 ? "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRu-CkRGLmZwwLcVZku_t0woWTro6hMcjF4ng&usqp=CAU"
                  : i = 5 ? "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTsnKibjpbmtpKj8QTj-fXv3uZH_jqsRBJow&usqp=CAU" : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRu-CkRGLmZwwLcVZku_t0woWTro6hMcjF4ng&usqp=CAU",


          })
        })
        setCat(arr)
      }
    } catch (e) {
      console.log(e)
    }


  }, [])
  console.log("cat", Cat)
  return (
    <>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
        <Text style={{ fontWeight: 'bold', marginLeft: 10 }}>Explore by Category</Text>
        <Text onPress={() => navigation.navigate('Categorydtails', { "data": Cat, 'HandleCategory': HandleCategory })} style={{ marginRight: 10, color: "grey" }}>See All({Cat.length})</Text>
      </View>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={Cat}
        keyExtractor={(item, index) => index.toString()}
        contentContainerStyle={{ marginTop: 15 }}
        renderItem={({ item }) => (
          <TouchableOpacity style={{ marginLeft: 10, marginRight: 10 }} onPress={() => HandleCategory(item.id, item.name,item.restros)} >
            <Image
              source={{ uri: item.icon }}
              style={{ width: 70, height: 70, borderRadius: 10 }}
            />
            <Text style={{ marginTop: 5, textAlign: "center" }}>{item.name}</Text>
          </TouchableOpacity>
        )}
      />
    </>
  )
}
