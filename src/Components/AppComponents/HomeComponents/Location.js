import React, { useState } from 'react'
import { View, Image, Text, SafeAreaView, StyleSheet, ScrollView, True, FlatList, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { setDeleveryLoccation } from '../../../Redux/Slice/LoginSlice'


const data = [
  {
    "img": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTFTJMRePPReEd9MXLqT3ioCkr_v35z23cAg&usqp=CAU",
    "name": "Home address",
    "address": "oxford st.No:2 street x12",
    "id": 1
  },
  {
    "img": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTFTJMRePPReEd9MXLqT3ioCkr_v35z23cAg&usqp=CAU",
    "name": "Office address",
    "address": "oxford st.No:2 street x12",
    "id": 2
  }
]
export default function Location() {
  const dispatch = useDispatch()
  const [select, setselect] = useState(1)
  const login = useSelector(state => state.login)
  const { DeleveryLocation } = login
  const HandleAdd = (item) => {
    // dispatch(setDeleveryLoccation(item))
    setselect(item.id)
  }

  return (
    <FlatList
      key={2}
      data={data}
      keyExtractor={(item, index) => index.toString()}
      numColumns={2}
      renderItem={({ item }) => (
        <View style={{ flex: 1, flexDirection: 'row',  width:150,margin:10,borderWidth: select == item.id ? 1:0,borderColor:"#f06b32",borderRadius:5 }}>
          <TouchableOpacity onPress={() => HandleAdd(item)}
            style={{ justifyContent: 'flex-start', flexDirection: 'row',margin:5 }}>
            <Image
              source={{ uri: item.img }}
              style={{ height: 50, width: 50, borderRadius: 10 }}
            />
            <View style={{ marginLeft: 10, marginTop: 3 }}>
              <Text style={{ fontWeight: 'bold' }}>{item.name}</Text>
              <Text style={{ marginRight: 60 }}>{item.address}</Text>
            </View>
          </TouchableOpacity>
        </View>
      )
      }
    />
  )
}
