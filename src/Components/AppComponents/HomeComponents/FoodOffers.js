import React from 'react'
import { SafeAreaView, StyleSheet, Image, View, Text } from "react-native";
import { windowWidth } from '../../../Util/Dimensions';
export default function FoodOffers({mg}) {
    return (
        <View style={{marginTop: 20,top:mg ? 0: -40,marginBottom:20}}>

            <Image
                style={{width:"95%",height:150,marginLeft:10,borderRadius:10}}
                resizeMode='stretch'
                source={require("../../../../assets/burger2.jpg")}
            />
            <View style={{position:'absolute',marginLeft:windowWidth/2}}>
          
                <Text style={{ marginTop:20,color:'pink'}}>Mega</Text>
                <Text style={{ color:'white',fontSize:30}}>Offer</Text>
              <View style={{flexDirection:'row',}}>
              <Text style={{ marginTop:10, color:'pink',fontSize:25}}>Rs.200 </Text>
              <Text style={{ marginTop:10, color:'white',fontSize:20,textDecorationLine:'line-through'}}>Rs.200 </Text>
              </View>
              
              <Text style={{ marginTop:30, color:'white'}}>*Available until 24 July 2021 </Text>
            </View>
           
            
          
        </View>
    )
}
