import React from 'react'
import Ionicons from 'react-native-vector-icons/Ionicons'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Entypo from 'react-native-vector-icons/Entypo'
import { View, Image, Text, SafeAreaView, StyleSheet, ScrollView, True ,ActivityIndicator} from 'react-native'
import { Avatar, Title, Caption, TouchableRipple } from 'react-native-paper'
export default function Header({mylocation}) {
    return (
        <View style={{ justifyContent: "space-between", flexDirection: 'row',margin:10 }}>
        <View style={{backgroundColor:'tomato',padding:5,flexDirection: 'row',borderRadius:50,alignItems: 'center'}}>
          <Ionicons name="md-location-outline" size={20} color="white" style={{marginLeft:5,}}/>
          <Text style={{ marginLeft: 5,marginRight:10, color: 'white',}} >{mylocation}</Text>
        </View>
        <Avatar.Image source={{ uri: "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500", }}
          size={40}
        />
      </View>

    )
}
