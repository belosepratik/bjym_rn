
import React from "react";
import { useState } from "react";
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from "react-native";

import Font from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const FoodData = [
    {
        "img": "https://b.zmtcdn.com/data/pictures/2/19667922/e345f08eecf07656e9e9121e2fa976b6_o2_featured_v2.jpg",
        "name": "Sandwich Restro",
        "time": "45 min"
    },
    {
        "img": "https://b.zmtcdn.com/data/pictures/2/19667922/e345f08eecf07656e9e9121e2fa976b6_o2_featured_v2.jpg",
        "name": "Cake Gallery",
        "time": "30 min"
    },
    {
        "img": "https://b.zmtcdn.com/data/pictures/2/19667922/e345f08eecf07656e9e9121e2fa976b6_o2_featured_v2.jpg",
        "name": "Sandwich ",
        "time": "60 min"
    },
    {
        "img": "https://b.zmtcdn.com/data/pictures/2/19667922/e345f08eecf07656e9e9121e2fa976b6_o2_featured_v2.jpg",
        "name": "Restro",
        "time": "45 min"
    },
    {
        "img": "https://b.zmtcdn.com/data/pictures/2/19667922/e345f08eecf07656e9e9121e2fa976b6_o2_featured_v2.jpg",
        "name": "Sandwich Restro",
        "time": "45 min"
    },
]


export default function Food({ HandleFood, data,navigation }) {
    // console.log(data.results)
    const [Restro, setRestro] = useState([])
    let arr = []
    React.useEffect(() => {
        try {
            if (data) {
                data.map((d, i) => {
                    arr.push({
                        name: d.name,
                        img: 'https://b.zmtcdn.com/data/pictures/2/19667922/e345f08eecf07656e9e9121e2fa976b6_o2_featured_v2.jpg',
                        address: `${d.addr1} ${d.addr2} ${d.addr3} ${d.addr4}`,
                        city: d.city,
                        country: d.country,
                        time: "45 min"

                    })
                })
                setRestro(arr)
            }
        } catch (e) {
            console.log(e)
        }

    }, [])
    return (
        <View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                <Text style={{ marginLeft: 10, fontSize: 20, fontWeight: 'bold' }}>Most Popular</Text>
                <Text onPress={()=>navigation.navigate('Resturentdetails',{"data":Restro})} style={{ marginRight: 10, fontSize: 16, color: 'grey' }}>View All</Text>
            </View>
            <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={Restro}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={{ marginTop: 15 }}
                renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => HandleFood(item.name, item.img,item.address)}
                        style={{ marginLeft: 10 }}>
                        <Image
                            style={{
                                width: 250,
                                height: 150,
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20,
                                marginRight: 10
                            }}
                            source={{
                                uri: item.img,
                            }}
                        >
                        </Image>
                        <View>
                            <Text style={{ margin: 10, fontSize: 20 }}>{item.name}</Text>
                            <View style={{ flexDirection: 'row', }}>
                                <Font style={{ marginLeft: 10, }} name="star" size={20} color="#ffe234" />
                                <Font style={{ marginLeft: 10, }} name="star" size={20} color="#ffe234" />
                                <Font style={{ marginLeft: 10, }} name="star" size={20} color="#ffe234" />
                                <Font style={{ marginLeft: 10, }} name="star" size={20} color="#ffe234" />
                                <Font style={{ marginLeft: 10, }} name="star" size={20} color="#ffe234" />
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                                <MaterialCommunityIcons name="clock-time-five-outline" size={20} color="grey" />
                                <Text style={{ color: 'grey', marginLeft: 5 }}>{item.time}</Text>
                            </View>

                        </View>

                    </TouchableOpacity>
                )}

            />
        </View>
    )
}
