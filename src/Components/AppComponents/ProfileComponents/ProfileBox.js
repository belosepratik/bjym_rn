import React from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  ScrollView,
  View,
  TextInput,
  Modal,
  Button,
  Animated,
  Image,
  TouchableOpacity,
} from 'react-native';
// import Constants from 'expo-constants';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';


export default function ProfileBox(props) {
  const { name, law, address, email, phone } = props;

  const [modal, setModal] = React.useState(false);

  const [visible, setVisible] = React.useState(false);
  return (
    <SafeAreaView>
      <View style={[styles.card]}>
        <Text style={{ marginTop: 20, marginLeft: 10, fontSize: 20, fontWeight: 'bold' }}>User Details</Text>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10,marginBottom:20 }}>
          <View>
            <Text style={{ fontSize: 16, }}>Name : {name}</Text>
            <Text style={{ fontSize: 16, marginTop: 10 }}>Email : {email}</Text>
            <Text style={{ fontSize: 16, marginTop: 10 }}>Phone Number : {phone}</Text>
            <Text style={{ fontSize: 16, marginTop: 10 }}>Address : {address}</Text>
          </View>
          <AntDesign name='edit' size={25} color="black" style={{marginRight:20}}/>
        </View>
      </View>

    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    // backgroundColor: "",
    //marginHorizontal: 20,
  },
  text: {
    fontSize: 42,
  },
  card: {
    // shadowColor: '#000000',
    // shadowOffsetWidth: 1,
    // shadowOffsetHeight: 8,
    // shadowOpacity: 0.32,
    // shadowRadius: 10.46,
    // padding: 5,
    margin: 10,
    borderRadius: 10,
    elevation: 15,
    backgroundColor: '#fff',
    marginHorizontal: 20,
  },
  img: {
    backgroundColor: 'white',
    borderRadius: 25,
    padding: 10,
    borderColor: 'rgb(37, 150, 190)',
    borderWidth: 3,
    marginTop: 20,
    marginLeft: 20,
  },
  info: {
    flexDirection: 'row',
    padding: 10,
  },

  // container: {
  //       flex: 1,
  //       alignItems: 'center',
  //       justifyContent: 'center',
  //       backgroundColor: '#8fcbbc',
  //     },
  modalBackGround: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    width: '80%',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 20,
    elevation: 20,
    height: '80%',
  },
  header: {
    width: '100%',
    height: 40,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  txtInputStyle: {
    fontSize: 20,
    borderBottomWidth: 1,
    borderColor: '#09b2f0',
  },
});