import React from 'react'
import { View, Text, Image, TouchableHighlight, StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import { windowHeight, windowWidth } from '../../../Util/Dimensions'
import HeartIcon from '../../Global/HeartIcon'

export default function FoodListSearchComponents({FootTab,HandleRestro}) {
    return (
    <View>
        <View>
        <FlatList
                data={FootTab}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={{ paddingTop: 20,backgroundColor:'white' }}
                renderItem={({ item }) => (
                    <>
                        <TouchableOpacity onPress={()=>HandleRestro(item)}
                         style={{ justifyContent: "space-between", flexDirection: 'row', margin: 10,marginTop:20 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../../../assets/food.jpeg')}
                                    style={{ height: 100, width: 100, borderRadius: 10 }}
                                />

                                <View style={{ marginLeft: 20, marginTop: 10 }}>
                                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{item.name}</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ color: 'grey', marginTop: 5 }}>{item.price}</Text>
                                        <Text style={{ color: 'grey', marginTop: 5, marginLeft: 10, textDecorationLine: 'line-through' }}>22,500</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
                                        <HeartIcon icon="MaterialCommunityIcons" name="percent" size={15} color="white" bg="#f06b32" wh={15} />
                                        <Text style={{ margin: 4 }}>{item.dev}</Text>
                                    </View>
                                </View>
                            </View>
                            <HeartIcon icon="Ionicons" name="heart-outline" size={25} color="#f06b32" />
                        </TouchableOpacity>
                        {
                            item.left &&
                            <View style={{
                                position: 'absolute', marginTop: windowHeight / 8,
                                left: windowWidth / 11, height: 30, width: 50, backgroundColor: '#f06b32',
                                alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderColor: 'white'
                            }}>
                                <Text style={{ color: 'white', }}>5 left</Text>
                            </View>
                        }

                        {/* <View style={{borderWidth:0.2,marginTop:10}}/> */}

                    </>
                )}
            />
        </View>
    </View>
    )
}
