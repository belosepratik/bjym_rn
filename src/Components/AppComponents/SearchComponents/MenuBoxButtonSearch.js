import React from 'react'
import {View, Text, StyleSheet, TouchableHighlight,TouchableOpacity } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
export default function MenuBoxButtonSearch({data,CRT,HandleSelect,select,HandleCat,cat}) {
  console.log("select",select)
    return (
      <View >
        <View style={styles.container}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={CRT}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <View style={{flexDirection:'row',marginBottom:10,marginTop:15}}>
              <TouchableOpacity onPress={()=>HandleSelect(item.id)}
               style={{padding:10,borderWidth:0.6,borderRadius:10,marginLeft:10,backgroundColor: select==item.id? '#f06b32':'white',}}>
                 <Text style={{color:select==item.id ? 'white' :'black'}}>{item.name}</Text></TouchableOpacity>
            </View>
            )}
          />
       
        {/* Seprator */}
        <View style={styles.seprator}></View>
  
        {/* Menu Buttons */}
        <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
            <View style={{flexDirection:'row',marginBottom:10,marginTop:15}}>
            <TouchableOpacity onPress={()=>HandleCat(item.id)}
             style={{padding:10,borderWidth:0.6,borderRadius:10,marginLeft:10,backgroundColor: cat==item.id? '#f06b32':'white',}}>
               <Text style={{color:cat==item.id ? 'white' :'black'}}>{item.name}</Text></TouchableOpacity>
            </View>

        )}
        />
     
      </View>
      <View style={{borderColor:'#DCDCDC',borderWidth:5}}/>
      </View>
    )
}


const styles=StyleSheet.create({
    container:{
      // width:'100%',
      margin:10,
    },
    activeBtn:{
    //   width:90,
      padding:5,
      borderRadius:15,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor:'#2C3E50',
    },
    btn:{
      width:90,
      padding:5,
      borderRadius:15,
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth:1,
      borderColor:'silver',
      marginLeft:8,
    },
    activeBtntx:{
      color:'white',
      fontWeight:'bold',
      padding:6

    },
    btntx:{
      fontWeight:'bold',
   
    },
    seprator:{
      backgroundColor:'silver',
      width:'100%',
      height:1,
      marginTop:10,
    },
    menuBtn:{
      color:'white',
      //fontWeight:'100',

      borderWidth:1,
      borderColor:'silver',
      padding:4,
      paddingLeft:10,
      paddingRight:10,
      borderRadius:5,
      marginLeft:8,
    },
    menubtntx:{
      fontWeight:'bold',
    }
  })
