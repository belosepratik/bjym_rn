import React from 'react'
import { Text, View, StatusBar } from 'react-native'
import Ent from 'react-native-vector-icons/Entypo'
export default function RatingsComponent({data}) {
    const { name, price, discount, rating, health, address, city, distance } = data
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 20, marginBottom: 15 }}>

            <View >
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Ent name="star" size={20} color="black" style={{ marginRight: 5 }} />
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{rating}</Text>
                </View>
                <Text style={{ color: 'grey' }}>100+ ratings</Text>
            </View>
            <View>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>45 mins</Text>
                <Text style={{ color: 'grey' }}>Delivery Timing</Text>
            </View>
            <View>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{price}</Text>
                <Text style={{ color: 'grey' }}>Cost for 2</Text>
            </View>

        </View>
    )
}
