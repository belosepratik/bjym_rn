import React, { useState } from 'react'
import { Text, View, StatusBar, TouchableOpacity } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import InputSpinner from 'react-native-input-spinner'
import Ent from 'react-native-vector-icons/Entypo'
import { useDispatch, useSelector } from 'react-redux'
import { setitems } from '../../../Redux/Slice/CartSlice'
export default function FoodComponents({ data, pname, pid }) {

    // console.log(pid)
    const dispatch = useDispatch()
    const cart = useSelector(state => state.cart)
    const { items } = cart
    const [childid, setchildid] = useState([])
    const [parentid, setparentid] = useState([])
    const [count, setcount] = useState(1)
    let cancel = [];
    let mainid = []
    let newArray = []
    React.useEffect(() => {
        if (items.length !== 0) {
            items.map((d, i) => {
                cancel.push(
                    d.id
                )
                mainid.push(d.pid)
            })
            setchildid(cancel)
            setparentid(mainid)
        } else {
            setchildid([])
            setparentid([])
        }
    }, [items])

    const HandleData = (item, name, id) => {
        let selects = [...items]

        selects.push({
            "pid": id,
            "name": name,
            "id": item.id,
            "item": item.item,
            "price": item.price,
            "orderPrice":item.price,
            "quantity": 1

        })
        dispatch(setitems(selects))

    }

    const HandleCancel = (item, name, id) => {
        const result = items
            .filter(function (element) {
                return element.id != item.id;
            });
        dispatch(setitems(result))
        // console.log(result)

    }
    return (
        <>
            <FlatList
                data={data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                    <View style={{ marginTop: 15, marginLeft: 10, marginBottom: 20 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Ent name="star" size={20} color="#f06b32" style={{ marginRight: 5 }} />
                            <Text style={{ fontSize: 16, color: '#f06b32' }}>{item.seller}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ marginTop: 5 }}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{item.item}</Text>
                                <Text style={{ fontSize: 20, color: 'grey', marginTop: 5 }}>₹{item.price}</Text>
                            </View>
                            {
                                parentid.includes(pid)
                                    &&
                                    childid.includes(item.id) ?
                                    <TouchableOpacity style={{ marginRight: 20, }} onPress={() => HandleCancel(item, pname, pid)}>
                                        <Text style={{
                                            fontSize: 20, paddingLeft: 30, paddingRight: 30,
                                            borderWidth: 0.3, paddingTop: 10, paddingBottom: 10, color: '#f06b32', fontWeight: 'bold'
                                        }}>
                                            CANCEL
                                        </Text>
                                    </TouchableOpacity>

                                    :
                                    <TouchableOpacity style={{ marginRight: 20, }} onPress={() => HandleData(item, pname, pid)} >
                                        <Text style={{
                                            fontSize: 20, paddingLeft: 30, paddingRight: 30,
                                            borderWidth: 0.3, paddingTop: 10, paddingBottom: 10, color: '#f06b32', fontWeight: 'bold'
                                        }}>
                                            ADD
                                        </Text>
                                    </TouchableOpacity>
                            }

                        </View>

                    </View>
                )}
            />

        </>
    )
}
