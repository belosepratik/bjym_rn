import React from 'react'
import { Text, View, StatusBar } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { List } from 'react-native-paper'
import FoodComponents from './FoodComponents'
export default function FoodAccordian({ data, }) {
    return (
        <View style={{ marginTop: 10 }}>
            {/* <List.AccordionGroup  > */}
                <FlatList
                    data={data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                        <View>

                            <View style={{ marginTop: 0 }}>

                                <List.Accordion title={`${item.name} (${item.food.length})`} id={item.id} titleStyle={{ fontWeight: 'bold', fontSize: 20, }} style={{ backgroundColor: 'white', }} >
                                    <View>
                                        <FoodComponents data={item.food} pname={item.name} pid={item.id}  />
                                    </View>
                                </List.Accordion>
                            </View>
                            <View style={{ borderWidth: 5, borderColor: '#F5F5F5', marginTop: 5 }} />
                        </View>
                    )}
                />
                {/* 
            <List.AccordionGroup  >
                <View >

                    <List.Accordion title="Recommented (1)" id="1" titleStyle={{ fontWeight: 'bold', fontSize: 20, }} style={{ backgroundColor: 'white', }}>
                        <View>
                            <FoodComponents />
                        </View>
                    </List.Accordion>
                </View>

                <View style={{ borderWidth: 10, borderColor: '#F5F5F5', marginTop: 20 }} />
                <View style={{ marginTop: 20 }}>

                    <List.Accordion title="Sandwiches (2) " id="2" titleStyle={{ fontWeight: 'bold', fontSize: 20, }} style={{ backgroundColor: 'white', }} >
                        <View>
                            <FoodComponents />
                            <FoodComponents />
                        </View>
                    </List.Accordion>
                </View>

                <View style={{ borderWidth: 10, borderColor: '#F5F5F5', marginTop: 20 }} />
                <View style={{ marginTop: 20 }}>
                    <List.Accordion title="Burger (3)" id="3" titleStyle={{ fontWeight: 'bold', fontSize: 20, }} style={{ backgroundColor: 'white', }}>
                        <View>
                            <FoodComponents />
                            <FoodComponents />
                            <FoodComponents />
                        </View>
                    </List.Accordion>
                </View>

                <View style={{ borderWidth: 10, borderColor: '#F5F5F5', marginTop: 20 }} />
                <View style={{ marginTop: 20 }}>
                    <List.Accordion title="Pizza (4)" id="4" titleStyle={{ fontWeight: 'bold', fontSize: 20, }} style={{ backgroundColor: 'white', }}>
                        <View>
                            <FoodComponents />
                            <FoodComponents />
                            <FoodComponents />
                            <FoodComponents />
                        </View>
                    </List.Accordion>
                </View>


            </List.AccordionGroup> */}
            {/* </List.AccordionGroup> */}
        </View>
    )
}
