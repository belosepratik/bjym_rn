import React from 'react'
import { View ,Text, TouchableOpacity} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
export default function CartPayFixedBottom({amount,HandlePayment,route,msg}) {
    // console.log(object)
    return (
      <View style={{marginBottom:route=='CartStack'?60:10}}>
          <View style={{backgroundColor:'#F8F8F8',flexDirection:'row',justifyContent:'space-between',margin:10,padding:15,alignItems:'center'}}>
              <View>
                  <View style={{flexDirection:'row'}}>
                  <Text>₹{amount}</Text>
                  <Text style={{color:'grey'}}> (with ₹20 tip) </Text>
                  </View>
             
              <Text style={{marginTop:5,color:'#0000EE'}}>View details To Bill </Text>
              </View>
             <TouchableOpacity onPress={()=>HandlePayment()} style={{backgroundColor:'#f06b32'}}>
                 <Text style={{padding:12,fontSize:18}}>
                     {msg}
                 </Text>
             </TouchableOpacity>
          </View>
      </View>
    )
}
