import React, { useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import InputSpinner from 'react-native-input-spinner';
import { useDispatch, useSelector } from 'react-redux';
import { setitems } from '../../../Redux/Slice/CartSlice';

export default function FoodQauntityComponent() {
    const [count, setcount] = useState(1)
    const dispatch = useDispatch()
    const cart = useSelector(state => state.cart)
    const { items } = cart
    console.log("items", items)
    let arr = []
    let objIndex = []
    return (

        <View>
            <FlatList
                data={items}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                    <>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10, marginTop: 20 }}>
                            <View style={{ width: '50%' }}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{item.item}</Text>
                                <Text style={{ color: 'grey' }}> Mini</Text>
                                <Text style={{ color: 'grey' }}> CUSTOMIZE</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '50%' }}>

                                <InputSpinner
                                    rounded={false}
                                    style={{ paddingTop: 0, marginRight: 20, }}
                                    colorLeft='#f06b32'
                                    colorRight='#f06b32'
                                    buttonStyle={{ elevation: 3 }}
                                    colorPress="#f06b32"
                                    buttonTextColor='black'
                                    max={10}
                                    min={0}
                                    step={1}
                                    colorMax={"#641652"}
                                    colorMin={"#641652"}
                                    value={item.quantity}
                                    onChange={(num) => {
                                        if (num == 0) {
                                            let data=items.filter(function (num) {
                                                return item.id != num.id
                                            })
                                            dispatch(setitems([]))
                                            dispatch(setitems(data))
                                        } else {
                                            items.map((d, i) => {
                                                arr.push({
                                                    pid: d.pid,
                                                    name: d.name,
                                                    id: d.id,
                                                    item: d.item,
                                                    price: d.price,
                                                    orderPrice: item.id == d.id ? d.price * num : d.orderPrice,
                                                    quantity: item.id == d.id ? num : d.quantity
                                                })

                                            })
                                            dispatch(setitems([]))
                                            dispatch(setitems(arr))


                                        }


                                    }}
                                />

                                <View style={{ justifyContent: 'center' }}>
                                    <Text style={{ color: 'grey', textDecorationLine: 'line-through' }}>₹206</Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 18 }}>₹{item.orderPrice}</Text>
                                </View>

                            </View>

                        </View>
                        <View style={{ marginTop: 10, borderWidth: 0.2 }} />
                    </>

                )}
            />
            <View style={{ marginTop: 10, borderWidth: 0.2 }} />
        </View>
    )
}
