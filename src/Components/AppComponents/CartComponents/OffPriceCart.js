import React from 'react'
import { View ,Text} from 'react-native'

export default function OffPriceCart() {
    return (
      <View style={{height:50,backgroundColor:'#bcf5bc',marginTop:20,justifyContent:'center',alignItems:'center'}}>
          <Text style={{fontSize:20}}>You have saved ₹51.5 on the bill</Text>
      </View>
    )
}
