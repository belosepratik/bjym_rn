import React from 'react'
import { View ,Text} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { useSelector } from 'react-redux'
export default function BillCart({amount}) {
    

    return (
       <View>
           <View style={{margin:10}}>
               <Text style={{marginLeft:10,fontWeight:'bold',fontSize:20}}>Bill Details</Text>
               <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:10,alignItems:'center'}}>
               <Text style={{marginLeft:10,fontSize:18}}>Item Total</Text>
               <Text style={{marginLeft:10}}>₹{amount}</Text>
               </View>
               <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:10,alignItems:'center'}}>
               <Text style={{marginLeft:10,fontSize:18}}>Total Discount </Text>
               <Text style={{marginLeft:10,color:'green'}}>- ₹51.00</Text>
               </View>
               <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:10,alignItems:'center'}}>
               <Text style={{marginLeft:10,fontSize:18}}>To Pay</Text>
               <Text style={{marginLeft:10}}>₹{amount}</Text>
               </View>
           </View>
       </View>
    )
}
