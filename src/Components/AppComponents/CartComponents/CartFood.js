import React from 'react'
import { Text,View,Image } from 'react-native'

export default function CartFood({name,address}) {
    return (
        <View style={{marginTop:10,flexDirection:'row'}}>
            <Image source={require('../../../../assets/food.jpeg')} style={{width:60,height:60,marginLeft:10}}/>
            <View>
            <Text style={{marginLeft:20,fontWeight:'bold',marginTop:10}}>{name}</Text>
            <Text style={{marginLeft:20,marginTop:5}}>{address}</Text>
            </View> 
        </View>

    )
}
