import React from 'react'
import { View ,Text} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
export default function ApplyCouponCart() {
    return (
       <View>
           <View style={{margin:10,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
               <View style={{flexDirection:'row',alignItems:'center'}}>
               <Ionicons name="add-circle" size={30}/>
            <Text style={{marginLeft:20,fontSize:20,fontWeight:'bold'}}>APPLY COUPON</Text>
               </View>
           <MaterialCommunityIcons name="greater-than" size={20}/>
           </View>
           <View style={{borderWidth:5,borderColor:'#DCDCDC'}}/>
       </View>
    )
}
