import React from 'react'
import { View, Text } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import Input from '../../Basic/Input';
export default function SuggestionCart() {
    return (
        <View>
            <View style={{flexDirection:'row',margin:10,alignItems:'center'}}>
                <AntDesign name="book" size={25} />
                <Input placeholder="Any resturents request? We will try out best to convey it " style={{ marginLeft: 20}} onChangeText={e => console.log(e)} />
            </View>
            <View style={{borderWidth:5,borderColor:'#DCDCDC'}}/>
        </View>
    )
}
