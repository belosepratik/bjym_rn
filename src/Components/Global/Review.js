import React from 'react'
import { Text, View } from 'react-native'
import Ent from 'react-native-vector-icons/FontAwesome'
export default function Review() {
    return (
        <View>
            <View style={{ alignItems: 'center', marginTop: 10 }}>
                <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Customer reviews</Text>
            </View>
            <View style={{ backgroundColor: '#f5f8ff', margin: 20, borderRadius: 10 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', padding: 5, alignItems: 'center' }}>
                    <Ent name='star' size={25} color='yellow' />
                    <Ent name='star' size={25} color='yellow' />
                    <Ent name='star' size={25} color='yellow' />
                    <Ent name='star' size={25} color='yellow' />
                    <Ent name='star' size={25} color='yellow' />
                    <Text style={{ fontSize: 18, marginLeft: 10 }}> 4.7 out of 5</Text>
                </View>

            </View>
            <Text style={{ alignSelf: 'center', top: -15 }}>40 customer rating</Text>
            <View style={{ alignSelf: 'center', marginLeft: 50 }}>

                <View style={{ flexDirection: 'row', marginTop: 20, }}>
                    <Text>5 star </Text>
                    <View style={{ backgroundColor: 'yellow', width: '50%', borderRadius: 20, marginLeft: 10 }} />
                    <Text style={{ marginLeft: 10 }}>87%</Text>
                </View>

                <View style={{ flexDirection: 'row', marginTop: 20, }}>
                    <Text>5 star </Text>
                    <View style={{ backgroundColor: 'yellow', width: '20%', borderRadius: 20, marginLeft: 10 }} />
                    <Text style={{ marginLeft: 10 }}>9%</Text>
                </View>


                <View style={{ flexDirection: 'row', marginTop: 20, }}>
                    <Text>5 star </Text>
                    <View style={{ backgroundColor: 'yellow', width: '15%', borderRadius: 20, marginLeft: 10 }} />
                    <Text style={{ marginLeft: 10 }}>4%</Text>
                </View>




                <View style={{ flexDirection: 'row', marginTop: 20, }}>
                    <Text>5 star </Text>
                    <View style={{ backgroundColor: 'yellow', width: '10%', borderRadius: 20, marginLeft: 10 }} />
                    <Text style={{ marginLeft: 10 }}>2%</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 20, }}>
                    <Text>5 star </Text>
                    <View style={{ backgroundColor: 'yellow', width: '5%', borderRadius: 20, marginLeft: 10 }} />
                    <Text style={{ marginLeft: 10 }}>1%</Text>
                </View>
            </View>


        </View>
    )
}
