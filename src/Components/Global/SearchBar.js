import React from 'react'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Entypo from 'react-native-vector-icons/Entypo'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { View, Image, Text, SafeAreaView, StyleSheet, ScrollView, True } from 'react-native'
import Input from '../Basic/Input'
export default function SearchBar({place,SearchTab,Handlesearch}) {
    return (
        <View style={{borderRadius:15,margin:10,flexDirection: 'row',alignItems:'center',
        justifyContent:'space-between', backgroundColor:'#e6e6e6'}}>
          <View style={{flexDirection: 'row',alignItems:'center',marginLeft:10}}>
            {
              SearchTab ? 
              <MaterialCommunityIcons name="chevron-left" size={25} color="grey"  style={{alignItems:'center'}}/>
              : 
              <EvilIcons name="search" size={25} color="grey"  style={{alignItems:'center'}} />
            }
         
          <Input placeholder={place ? place :`Search in thousands of products` } style={{marginLeft:20,width:'78%'}} onChangeText={e=>Handlesearch ? Handlesearch(e) : console.log(e)}/>
          </View>
          {
            SearchTab? 
            <Entypo name="cross" size={25} color="grey"  style={{marginRight:20}}/>
            :<Ionicons name="ios-mic-outline" color="grey"  size={25} style={{marginRight:20}}/>
          }
          
        </View>
    )
}
