import React from 'react'
import { Text, ScrollView, TouchableOpacity, View } from 'react-native'
export default function BottomCartButton({ noitem, Messgae, price, HandleAdd, page,tab }) {

    return (
        <View style={{ marginLeft: 10, marginRight: 10, marginBottom:tab? 80:10}}>
            <TouchableOpacity onPress={() => HandleAdd && HandleAdd()}
                style={{ justifyContent: 'space-around', borderRadius: 5, flexDirection: 'row', height: 50, alignItems: 'center', backgroundColor: '#f06b32' }} >
                {page == "hometocart" ? <Text style={{ fontSize: 20, color: 'white' }}>{`${Messgae} (${noitem})`}</Text>
                    : <>
                        <Text style={{ fontSize: 20, color: 'white' }}>{noitem} item</Text>
                        <Text style={{ fontSize: 20, color: 'white' }}>{Messgae}</Text></>
                }

                {
                    price && <Text style={{ fontSize: 20, color: 'white' }}>{price}</Text>
                }

            </TouchableOpacity>
        </View>
    )
}
