import React from 'react'
import Icon from 'react-native-vector-icons/Entypo'
import { Text, View, StatusBar } from 'react-native'
import Ent from 'react-native-vector-icons/Entypo'
import AntDesign from 'react-native-vector-icons/AntDesign';
export default function CommonHeader({ navigation, name, back }) {
    return (
        <>
            <View style={{ borderWidth: 0.2 }} />
            <View style={{ height: 50, justifyContent: back ?'space-between':'center', flexDirection: 'row', backgroundColor: 'white' }}>
                {
                    back && <AntDesign name="arrowleft" color="black" size={30} style={{ alignSelf: 'center', marginLeft: 10 }}
                        onPress={() => navigation.goBack()} />
                }


                <Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: 'bold' }}>{name}</Text>
                {back && <Icon name="dots-three-vertical" color="black" size={25} style={{ alignSelf: 'center', marginRight: 10 }} />}
            </View>
            <View style={{ borderWidth: 0.2 }} />
        </>
    )
}
