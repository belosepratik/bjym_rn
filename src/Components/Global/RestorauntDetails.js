import React from 'react'
import { View, Text, Image, TouchableHighlight, StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import Ion from 'react-native-vector-icons/Ionicons'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import MIcon from 'react-native-vector-icons/MaterialIcons'
export default function RestorauntDetails({ name, address }) {
    return (
        <View style={{ backgroundColor: 'white', margin: 20, marginTop: 20, borderRadius: 10, borderWidth: 0.4, elevation: 5 }}>
            <Text style={{ fontSize: 25, marginTop: 20, fontWeight: 'bold', alignSelf: 'center' }}>{name}</Text>
            <Text style={{ fontSize: 18, color: 'grey', alignSelf: 'center', marginTop: 10 }}>{address}</Text>
            <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 10 }}>
                <Text >Open</Text>
                <Text style={{ color: 'grey' }}> 8 am - 8pm</Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'center', marginBottom: 20 }}>
                <View style={{ flexDirection: 'row', }}>
                    <EvilIcons name='location' size={15} color='#f06b32' />
                    <Text style={{ marginLeft: 5 }}>1km</Text>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                    <Ion name="star-outline" size={15} color='#f06b32' />
                    <Text style={{ marginLeft: 5 }}>5.0</Text>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                    <MIcon name="verified-user" size={15} color='#f06b32' />
                    <Text style={{ marginLeft: 5 }}>Verified</Text>
                </View>


            </View>
        </View>

    )
}
