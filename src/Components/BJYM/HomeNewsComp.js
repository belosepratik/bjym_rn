import React from 'react'
import { Text, View, Image, FlatList } from 'react-native'
import { news } from '../../../assets'
import { windowHeight, windowWidth } from '../../Util/Dimensions'
const newsdata = [
    {
        id: 1,
        name: 'karnataka chief Sri. B.S Yediyurappa',
        img: require('../../../assets/news1.jpg'),
        data: 'NEW DELHI: Whether or not Karnataka chief minister BS Yediyurappa steps down from his post on July 26, he has already created a record of sorts in the BJP, which is at the helm at the centre and in the state. His record would get further strengthened if he continues as the state CM beyond the date when a meeting of BJP legislature party takes place.Of the 30 CMs in the country, 18 belong to the NDA of which the BJP is one of the leading constituents. Yediyurappa, at 78 years of age, is the oldest among them. Among the 30 CMs, Yediyurappa is younger than just Amarinder Singh of Punjab who is 79 years old.'
    },
    {
        id: 2,
        name: 'Katrina Kaif',
        img: require('../../../assets/kat.jpeg'),
        data: 'Florals are a versatile print. You can wear them in any season and also any occasion. If you need some celeb inspiration, Katrina Kaif‘s latest outings is what you need to check out. The actor was recently spotted looking lovely in a floral printed co-ord set from the brand Summer Somewhere which consisted of a crop top with softly elasticated sleeves that can be worn on or off the shoulder teamed with comfortable high-waist Fiji shorts.The look, styled by Ami Patel, was completed with minimalist gold earrings, a gold chain, and a messy ponytail.'
    },
    {
        id: 3,
        name: 'karnataka chief Sri. B.S Yediyurappa',
        img: require('../../../assets/news1.jpg'),
        data: 'NEW DELHI: Whether or not Karnataka chief minister BS Yediyurappa steps down from his post on July 26, he has already created a record of sorts in the BJP, which is at the helm at the centre and in the state. His record would get further strengthened if he continues as the state CM beyond the date when a meeting of BJP legislature party takes place.Of the 30 CMs in the country, 18 belong to the NDA of which the BJP is one of the leading constituents. Yediyurappa, at 78 years of age, is the oldest among them. Among the 30 CMs, Yediyurappa is younger than just Amarinder Singh of Punjab who is 79 years old.'
 }
]

export default function HomeNewsComp() {

    return (
        <View>
            <Text style={{marginTop:10,fontSize:25,marginLeft:20}}>News</Text>
            <View style={{ margin: 10 }}>
                <FlatList
                    data={newsdata}
                    keyExtractor={(item, index) => index.toString()}
                    contentContainerStyle={{ marginTop: 0 }}
                    renderItem={({ item }) => (
                        <View>
                            <Image source={item.img} resizeMode='stretch' style={{ width: windowWidth/1.1, height: windowHeight/4,borderRadius:10 }} />
                           <Text style={{alignSelf:'flex-end',marginRight:30,fontSize:12}}>23 Jan 2021 8.00 pm</Text>
                            <Text style={{fontSize:16,fontWeight:'bold',marginTop:5}}>{item.name}</Text>
                            <Text style={{marginRight:10,marginBottom:10}}>{item.data}</Text>
                            <View style={{marginTop:10,marginBottom:10,borderWidth:0.2}}/>
                        </View>
                    )}
                />
            </View>
        </View>
    )
}
