import React from 'react';
import {ImageBackground, Text, View,Image} from 'react-native';
import {windowWidth} from '../../Util/Dimensions';

export default function  Banner() {
  return (
    <View style={{marginRight: 0, marginLeft: 0, marginTop: 0,flexDirection:'column'
    ,justifyContent: 'space-between'}}>
     <View style={{justifyContent: 'space-evenly',alignItems: 'center',flexDirection:'row'}}>
     <View style={{
                backgroundColor: 'white',
                height: 100,
                width: 100,
                borderRadius: 60,
                marginTop: 2,
                alignItems: 'center',
                justifyContent: 'center',
                elevation: 5,
              }}>
          <Image
                source={require('../../../assets/nitingadkari.png')}
                style={{width: 80, height: 80, borderRadius: 50,}}
              />
          </View>
          <View style={{
                backgroundColor: 'white',
                height: 100,
                width: 100,
                borderRadius: 50,
                marginTop:1,
                alignItems: 'center',
                justifyContent: 'center',
                elevation: 5,
                
              }}>
          <Image
                source={require('../../../assets/deve.jpg')}
                style={{width: 80, height: 80, borderRadius: 50}}
              />
          </View>
          </View>
    <View style={{flexDirection: 'row',justifyContent: 'space-evenly',marginTop:13}}>
      <View style={{
                backgroundColor: 'white',
                height: 100,
                width: 100,
                borderRadius: 50,
                marginTop:1,
                alignItems: 'center',
                justifyContent: 'center',
                elevation: 5,
                
              }}>
          <Image
                source={require('../../../assets/mohan.jpg')}
                style={{width: 80, height: 80, borderRadius: 50}}
              />
          </View>
     
  
          <View style={{
                backgroundColor: 'white',
                height: 100,
                width: 100,
                borderRadius: 50,
                marginTop:1,
                alignItems: 'center',
                justifyContent: 'center',
                elevation: 5,
                
              }}>
          <Image
                source={require('../../../assets/ParendraPatle.jpeg')}
                style={{width: 80, height: 80, borderRadius: 50}}
              />
          </View>
          </View>
          
    </View>
  );
}
