/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';
import { store, persistor } from './src//Redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';
import Navigation from './src/Routes/Navigation';
import ChangePassword from './src/Routes/ChangePassword/ChangePassword';
import ChangeContact from './src/Routes/ChangeContact/ChangeContact';
import EditProfile from './src/Routes/EditProfile/EditProfile'
import SplashScreen from './src/Views/SplashScreen';
import About from './src/Views/About/About';
const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <PaperProvider>
          <Navigation />
          {/* <ChangePassword /> */}
          {/* <ChangeContact /> */}
          {/* <EditProfile /> */}
          {/* <SplashScreen /> */}
          {/* <About />  */}
        </PaperProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
